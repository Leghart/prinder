from pydantic import validator

from app.database import get_main_tables

from .base import BaseConfig


class PermissionBase(BaseConfig):
    """Base permission schema configuration.

    Should be overriden to get CRUD operations and others.
    """

    name: str
    table: str


class PermissionCreateSchema(PermissionBase):
    """Schema representing Create from CRUD.

    As it only contains basic fields, it is the pure Base class.
    """

    @validator("table")
    def table_must_exist(cls, value):
        """Validates requested table."""
        if value not in get_main_tables():
            raise ValueError("Requested table does not exist")

        return value


class PermissionReadSchema(PermissionBase):
    """Schema representing Read from CRUD.

    Adds object's id.
    """

    id: int
