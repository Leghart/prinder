from __future__ import annotations

from pydantic import Field

from .base import BaseConfig


class SkillBase(BaseConfig):
    """Base skill schema configuration.

    Should be overriden to get CRUD operations and others.
    """

    name: str
    level: int = Field(..., gt=0, lt=6)


class SkillCreateSchema(SkillBase):
    """Schema representing Create from CRUD.

    Contains user_id because evry skill cac has only one user and
    skill without user is forbidden.
    """

    user_id: int


class SkillReadSchema(SkillCreateSchema):
    """Schema representing Read from CRUD.

    Adds object's id to schema which representes a data set for
    Create method.
    """

    id: int


class SkillUpdateSchema(BaseConfig):
    """Schema representing Update from CRUD.

    Allows to modify some fields (PATCH method).
    """

    name: str | None = None
    level: int | None = None


class SkillIDSchema(SkillBase):
    """Special schema to related relations.

    Contains only basic information about object without user_id
    (this schema will be used mainly in User representation to reduce redundant
    informations).
    """

    id: int
