from pydantic import BaseModel


class BaseExceptionSchema(BaseModel):
    detail: str


class NotFoundSchema(BaseExceptionSchema):
    ...


class ConflictSchema(BaseExceptionSchema):
    ...


class UnauthorizedSchema(BaseExceptionSchema):
    detail: str = "Could not validate credentials"


class BadRequestSchema(BaseExceptionSchema):
    ...


class NotAcceptableSchema(BaseExceptionSchema):
    ...
