from __future__ import annotations

from .base import BaseConfig


class TechnologyBase(BaseConfig):
    """Base technology schema configuration.

    Should be overriden to get CRUD operations and others.
    """

    name: str
    version: str


class TechnologyCreateSchema(TechnologyBase):
    """Schema representing Create from CRUD.

    Contains `project_id` because every technology has only one project and
    technology without project is forbidden.
    """

    project_id: int


class TechnologyReadSchema(TechnologyCreateSchema):
    """Schema representing Read from CRUD.

    Adds object's id to schema which representes a dataset for
    Create method.
    """

    id: int


class TechnologyReadRelatedSchema(TechnologyBase):
    """Special schema representing ReadRelated.

    Contains information from `Read` schema, without `project_id`,
    because will be used in Project representation.
    """

    id: int


class TechnologyUpdateSchema(BaseConfig):
    """Schema representing Update from CRUD.

    Allows to modify some fields (PATCH method).
    """

    name: str | None = None
    version: str | None = None


class TechnologyIDSchema(TechnologyBase):
    """Special schema to related relations.

    Contains only `id` which refers to object.
    """

    id: int
