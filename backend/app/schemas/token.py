import pydantic


class TokenSchema(pydantic.BaseModel):
    access_token: str
    token_type: str


class TokenExtraDataSchema(pydantic.BaseModel):
    username: str
    scopes: list[str]
