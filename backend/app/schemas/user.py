from __future__ import annotations

from .base import BaseConfig


class UserBase(BaseConfig):
    """Base user schema configuration.

    Should be overriden to get CRUD operations and others.
    Despite simple model fields, contains relation to other tables like
    `languages` and `users`.
    """

    first_name: str
    last_name: str
    email: str
    username: str
    bio: str | None = None


class UserCreateSchema(UserBase):
    """Schema representing Create from CRUD.

    Contains additonal necessary `password` field which is encrypted
    to bytes form later.
    """

    password: str


class CreateHashedSchema(UserBase):
    """Special schema to represent create data with encrypted password."""

    password: bytes


class UserReadSchema(UserBase):
    """Schema representing Read from CRUD.

    Adds object's id to base representation.
    """

    id: int
    languages: list["LanguageReadSchema"]
    skills: list["SkillIDSchema"]
    owned: list["ProjectInfoSchema"]
    contributions: list["ProjectInfoSchema"]
    liked_projects: list["ProjectInfoSchema"]
    image: str | None = None


class UserUpdateSchema(BaseConfig):
    """Schema representing Update from CRUD.

    Allows to modify some fields (PATCH method).
    """

    first_name: str | None = None
    last_name: str | None = None
    email: str | None = None
    bio: str | None = None
    username: str | None = None
    password: str | None = None


class UpdateHashedSchema(UserUpdateSchema):
    """Special schema to represent update data with enceypted password.

    There is type-ignore to allow override parent class to avoid
    repeating unnecessaries part of code.
    """

    password: bytes | None = None  # type: ignore [assignment]


class UserIDSchema(BaseConfig):
    """Special schema to represent user only by id.

    Used by related tables like `languages`, `skills` etc.
    """

    id: int


class UserPermissionsSchema(UserReadSchema):
    """Special schema which overrides Read user with his permissions."""

    permissions: list["PermissionReadSchema"]


class ToToken(CreateHashedSchema):
    """Special schema to add permissions to JWT."""

    permissions: list["PermissionReadSchema"]
    is_superuser: bool
    id: int


from .language import LanguageReadSchema
from .permission import PermissionReadSchema
from .project import ProjectInfoSchema
from .skill import SkillIDSchema

UserReadSchema.update_forward_refs()
UserCreateSchema.update_forward_refs()
UserPermissionsSchema.update_forward_refs()
ToToken.update_forward_refs()
