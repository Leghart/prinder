import pydantic


class BaseConfig(pydantic.BaseModel):
    class Config:
        orm_mode = True
