from datetime import date
from typing import Optional

from ..models.project import Project
from .base import BaseConfig


class ProjectBase(BaseConfig):
    """Base language schema configuration.

    Should be overriden to get CRUD operations and others.
    """

    name: str
    url: str | None
    description: str | None


class ProjectCreateSchema(ProjectBase):
    """Schema representing Create from CRUD."""

    start_date: str
    end_date: str
    owner_id: int
    language_id: int


class ProjectReadSchema(ProjectBase):
    """Schema representing Read from CRUD."""

    id: int
    owner: Optional["UserIDSchema"]
    language: Optional["LanguageReadSchema"]
    collaborators: list["UserIDSchema"]
    liked_by: list["UserIDSchema"]
    technologies: list["TechnologyReadRelatedSchema"]
    start_date: date
    end_date: date
    archived: bool

    class Config:
        json_encoders = {date: lambda dt: dt.strftime(Project.DATE_FORMAT)}


class ProjectUpdateSchema(BaseConfig):
    """Schema representing Update from CRUD.

    Allows to modify some fields (PATCH method).
    """

    name: str | None = None
    archived: bool | None = None
    url: str | None = None
    description: str | None = None
    start_date: str | None = None
    end_date: str | None = None
    language_id: int | None = None


class ProjectInfoSchema(BaseConfig):
    """Special schema to represent short informaction about project.

    Used by related tables.
    """

    id: int
    name: str
    description: str | None = None


from .language import LanguageReadSchema
from .technology import TechnologyReadRelatedSchema
from .user import UserIDSchema

ProjectReadSchema.update_forward_refs()
