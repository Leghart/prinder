from .base import BaseConfig


class LanguageBase(BaseConfig):
    """Base language schema configuration.

    Should be overriden to get CRUD operations and others.
    """

    name: str
    level: str


class LanguageCreateSchema(LanguageBase):
    """Schema representing Create from CRUD.

    As it only contains basic fields, it is the pure Base class.
    """


class LanguageReadSchema(LanguageBase):
    """Schema representing Read from CRUD.

    Adds object's id.
    """

    id: int


class LanguageUpdateSchema(BaseConfig):
    """Schema representing Update from CRUD.

    Allows to modify some fields (PATCH method).
    """

    name: str | None = None
    level: str | None = None


class LanguageIDSchema(BaseConfig):
    """Special schema to related relations.

    Contains only basic information about object without list of users
    (this schema will be used mainly in User representation to reduce redundant
    informations).
    """

    id: int


class LanguageWithUsers(LanguageReadSchema):
    """Special schema to related relations.

    Shows which users have relation to this record.
    """

    users: "list[UserIDSchema]"


from .user import UserIDSchema

LanguageReadSchema.update_forward_refs()
