from __future__ import annotations

from sqlalchemy import Column, Integer, String, UniqueConstraint
from sqlalchemy.orm import Mapped, relationship

from app.database import Base

from .base_model import SqlBaseModel


class Permission(Base, SqlBaseModel):
    __tablename__ = "permissions"

    id = Column(Integer, primary_key=True)
    table = Column(String(30))
    name = Column(String(30))

    users: Mapped[list] = relationship("User", secondary="user_permission", back_populates="permissions")

    __table_args__ = (UniqueConstraint("table", "name", name="_permission_uc"),)

    def __str__(self) -> str:
        return f"{self.table}:{self.name}"
