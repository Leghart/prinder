from __future__ import annotations

from typing import Optional

from sqlalchemy import Boolean, Column, DateTime, Integer, LargeBinary, String, Text
from sqlalchemy.orm import Mapped, relationship
from sqlalchemy.sql import func

from app.database import Base
from app.models.permissions import Permission

from .base_model import SqlBaseModel


class User(Base, SqlBaseModel):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    first_name = Column(String(20))
    last_name = Column(String(20))
    email = Column(String(30), unique=True, nullable=False)
    bio = Column(Text, nullable=True, default=None)
    image = Column(String, nullable=True, default=None)

    created_at = Column(DateTime(timezone=True), server_default=func.now())
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())

    username = Column(String(20), nullable=False, unique=True)
    password = Column(LargeBinary(60))

    languages: Mapped[list[int]] = relationship("Language", secondary="user_language")
    contributions: Mapped[list[int]] = relationship(
        "Project", secondary="user_project", back_populates="collaborators"
    )
    liked_projects: Mapped[list[int]] = relationship(
        "Project",
        secondary="projects_liked_by_users",
        back_populates="liked_by",
    )

    skills: Mapped[list[int]] = relationship("Skill", back_populates="user", cascade="all, delete")
    owned: Mapped[list[int]] = relationship("Project", back_populates="owner")

    permissions: Mapped[list[int]] = relationship("Permission", secondary="user_permission")
    is_superuser = Column(Boolean, default=False)

    def has_perm(self, permission: Optional[Permission]) -> bool:
        """Checks if user has chosen permission.

        These permissions are stored as m2m relation to table
        `permisssions`.
        """

        if not permission:
            return False

        if permission not in self.permissions:
            return False

        return True

    def __str__(self) -> str:
        return self.username
