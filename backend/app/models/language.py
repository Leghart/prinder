from sqlalchemy import Column, DateTime, Integer, String, UniqueConstraint
from sqlalchemy.orm import Mapped, relationship
from sqlalchemy.sql import func

from app.database import Base

from .base_model import SqlBaseModel


class Language(Base, SqlBaseModel):
    __tablename__ = "languages"

    id = Column(Integer, primary_key=True)
    name = Column(String(20))
    level = Column(String(2))
    users: Mapped[list] = relationship("User", secondary="user_language", back_populates="languages")

    created_at = Column(DateTime(timezone=True), server_default=func.now())
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())

    projects: Mapped[list] = relationship("Project", back_populates="language")

    __table_args__ = (UniqueConstraint("level", "name", name="_language_uc"),)

    def __str__(self) -> str:
        return f"{self.name}-{self.level}"
