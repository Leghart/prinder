from sqlalchemy import Column, ForeignKey, Integer

from app.database import Base


class UserLanguage(Base):
    __tablename__ = "user_language"
    user_id = Column(Integer, ForeignKey("users.id"), primary_key=True)
    language_id = Column(Integer, ForeignKey("languages.id"), primary_key=True)


class UserProject(Base):
    __tablename__ = "user_project"
    user_id = Column(Integer, ForeignKey("users.id"), primary_key=True)
    project_id = Column(Integer, ForeignKey("projects.id"), primary_key=True)


class ProjectsLikedByUsers(Base):
    __tablename__ = "projects_liked_by_users"
    user_id = Column(Integer, ForeignKey("users.id"), primary_key=True)
    project_id = Column(Integer, ForeignKey("projects.id"), primary_key=True)


class UserPermission(Base):
    __tablename__ = "user_permission"
    user_id = Column(Integer, ForeignKey("users.id"), primary_key=True)
    permission_id = Column(Integer, ForeignKey("permissions.id"), primary_key=True)
