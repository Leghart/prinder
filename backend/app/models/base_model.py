from typing import Container, Optional, Type

from pydantic import BaseConfig, BaseModel, create_model
from sqlalchemy.inspection import inspect
from sqlalchemy.orm.properties import ColumnProperty

from app.database import SessionLocal


class classproperty(property):
    def __get__(self, obj, objtype=None):
        return super(classproperty, self).__get__(objtype)

    def __set__(self, obj, value):
        super(classproperty, self).__set__(type(obj), value)


class OrmConfig(BaseConfig):
    orm_mode = True


class SqlBaseModel:
    @classproperty
    def session(cls):
        if not hasattr(cls, "_session"):
            return SessionLocal

        return cls._session

    @session.setter  # type: ignore [no-redef]
    def session(cls, value):
        cls._session = value

    def to_pydantic(
        self, *, config: Type = OrmConfig, exclude: Container[str] = ["created_at", "updated_at"]
    ) -> Type[BaseModel]:
        """Translates SQLAlchemy object into pydantic class.

                        !!! Important !!!
        Metod gets EVERY field stored in database, so hidden data
        leakage is possible. To avoid this, add sensitive data to
        `exclude` list.
        """
        db_model = self._sa_instance_state.class_  # type:ignore [attr-defined]
        mapper = inspect(db_model)
        fields = {}
        for attr in mapper.attrs:
            if isinstance(attr, ColumnProperty):
                if attr.columns:
                    name = attr.key
                    if name in exclude:
                        continue
                    column = attr.columns[0]
                    python_type: Optional[type] = None
                    if hasattr(column.type, "impl"):
                        if hasattr(column.type.impl, "python_type"):
                            python_type = column.type.impl.python_type
                    elif hasattr(column.type, "python_type"):
                        python_type = column.type.python_type
                    assert python_type, f"Could not infer python_type for {column}"
                    default = None
                    if column.default is None and not column.nullable:
                        default = ...
                    fields[name] = (python_type, default)

        pydantic_model = create_model(db_model.__name__, __config__=config, **fields)  # type: ignore

        return pydantic_model.from_orm(self)
