from __future__ import annotations

from datetime import date, datetime
from typing import NoReturn

from sqlalchemy import Boolean, Column, DateTime, ForeignKey, Integer, String, Text
from sqlalchemy.orm import Mapped, relationship
from sqlalchemy.sql import func

from app.database import Base

from .base_model import SqlBaseModel


class Project(Base, SqlBaseModel):
    __tablename__ = "projects"
    DATE_FORMAT = "%d-%m-%Y"

    id = Column(Integer, primary_key=True)
    name = Column(String(30), unique=True)
    start_date = Column(DateTime(timezone=True), server_default=func.now())
    end_date = Column(DateTime(timezone=True), nullable=True, default=None)
    description = Column(Text, nullable=True, default=None)
    url = Column(Text, nullable=True, default=None)
    archived = Column(Boolean, default=False)

    created_at = Column(DateTime(timezone=True), server_default=func.now())
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())

    owner_id = Column(Integer, ForeignKey("users.id"))
    owner: Mapped["User"] = relationship("User", back_populates="owned")

    language_id = Column(Integer, ForeignKey("languages.id"))
    language: Mapped["Language"] = relationship("Language", back_populates="projects")

    collaborators: Mapped[list[int]] = relationship("User", secondary="user_project", back_populates="contributions")

    liked_by: Mapped[list[int]] = relationship(
        "User",
        secondary="projects_liked_by_users",
        back_populates="liked_projects",
    )

    technologies: Mapped[list[int]] = relationship("Technology", back_populates="project", cascade="all, delete")

    @classmethod
    def parse_date(cls, date: str) -> NoReturn | date:
        return datetime.strptime(date, cls.DATE_FORMAT).date()

    def __str__(self) -> str:
        return self.name


from .language import Language
from .user import User
