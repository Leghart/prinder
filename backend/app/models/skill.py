from sqlalchemy import Column, DateTime, ForeignKey, Integer, String
from sqlalchemy.orm import Mapped, relationship
from sqlalchemy.sql import func

from app.database import Base

from .base_model import SqlBaseModel


class Skill(Base, SqlBaseModel):
    __tablename__ = "skills"

    id = Column(Integer, primary_key=True)
    name = Column(String(30))
    level = Column(Integer)

    user_id = Column(Integer, ForeignKey("users.id"))
    user: Mapped[list] = relationship("User", back_populates="skills")

    created_at = Column(DateTime(timezone=True), server_default=func.now())
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())

    def __str__(self) -> str:
        return f"{self.name}-{self.level}"
