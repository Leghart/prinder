from sqlalchemy import Column, DateTime, ForeignKey, Integer, String
from sqlalchemy.orm import Mapped, relationship
from sqlalchemy.sql import func

from app.database import Base

from .base_model import SqlBaseModel


class Technology(Base, SqlBaseModel):
    __tablename__ = "technologies"

    id = Column(Integer, primary_key=True)
    name = Column(String(30))
    version = Column(String(10))

    project_id = Column(Integer, ForeignKey("projects.id"))
    project: Mapped[list] = relationship("Project", back_populates="technologies")

    created_at = Column(DateTime(timezone=True), server_default=func.now())
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())

    def __str__(self) -> str:
        return f"{self.name} ({self.version})"
