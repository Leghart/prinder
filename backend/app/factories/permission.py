import factory
from faker import Faker

from app.database import SessionLocal
from app.models import Permission

fake = Faker()


class PermissionFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        sqlalchemy_session = SessionLocal
        sqlalchemy_session_persistence = "commit"
        model = Permission

    table = factory.Faker("random_element", elements=["users", "projects", "skills"])
    name = factory.Faker("random_element", elements=["read", "update", "delete", "create"])
