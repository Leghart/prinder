import factory

from app.database import SessionLocal
from app.models import Skill


class SkillFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        sqlalchemy_session = SessionLocal
        sqlalchemy_session_persistence = "commit"
        sqlalchemy_get_or_create = ("name", "level")

        model = Skill

    name = factory.Faker(
        "random_element",
        elements=[
            "Django",
            "Python",
            "Vue",
            "React",
            "JavaScript",
            "PHP",
            "C++",
            "C#",
            "FastAPI",
            "Docker",
        ],
    )
    level = factory.Faker("pyint", min_value=1, max_value=5)

    @factory.post_generation
    def user(self, create, extracted, **kwargs):
        from .user import UserFactory

        session = self.session

        klass = UserFactory
        klass._meta.sqlalchemy_session = session

        if not create:
            return

        if extracted:
            self.user = extracted
        else:
            self.user = klass()

        session.commit()
