import factory
from faker import Faker

from app.database import SessionLocal
from app.models import Technology

fake = Faker()


class TechnologyFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        sqlalchemy_session = SessionLocal
        sqlalchemy_session_persistence = "commit"
        model = Technology

    name = factory.Faker(
        "random_element",
        elements=[
            "Django",
            "Vue",
            "Flask",
            "Tornado",
            "React",
            "FastAPI",
            "Docker",
        ],
    )

    @factory.lazy_attribute
    def version(self):
        return (
            f"{fake.pyint(min_value=0, max_value=20)}."
            f"{fake.pyint(min_value=0, max_value=20)}."
            f"{fake.pyint(min_value=0, max_value=20)}"
        )

    @factory.post_generation
    def project(self, create, extracted, **kwargs):
        from .project import ProjectFactory

        session = self.session

        klass = ProjectFactory
        klass._meta.sqlalchemy_session = session

        if not create:
            return

        if extracted:
            self.project = extracted
        else:
            self.project = klass()

        session.commit()
