from random import randint

import factory
from faker import Faker
from sqlalchemy.exc import IntegrityError

from app.database import SessionLocal
from app.models import User
from app.utils.auth import encrypt_password

from .language import LanguageFactory
from .permission import PermissionFactory
from .skill import SkillFactory

fake = Faker()


class UserFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        sqlalchemy_session = SessionLocal
        sqlalchemy_session_persistence = "commit"
        model = User

    @staticmethod
    def generate_username(*args):
        return fake.profile(fields=["username"])["username"]

    @factory.lazy_attribute
    def bio(self):
        if words := randint(10, 100) <= 80:
            return fake.paragraph(nb_sentences=words)

    @factory.lazy_attribute
    def password(self):
        return encrypt_password(fake.password()[-20:])

    first_name = factory.Faker("first_name")
    last_name = factory.Faker("last_name")
    username = factory.LazyAttribute(generate_username)
    email = factory.LazyAttribute(lambda _: fake.email()[-30:])
    is_superuser = False

    @factory.post_generation
    def add_languages(self, create, extracted, **kwargs):
        session = self.session

        klass = LanguageFactory
        klass._meta.sqlalchemy_session = session

        if not create:
            return

        if extracted:
            for language in extracted:
                try:
                    self.languages.append(language)
                    session.commit()

                except IntegrityError:
                    session.rollback()

    @factory.post_generation
    def add_skills(self, create, extracted, **kwargs):
        session = self.session

        klass = SkillFactory
        klass._meta.sqlalchemy_session = session

        if not create:
            return

        if extracted:
            for skill in extracted:
                self.skills.append(skill)

        session.commit()

    @factory.post_generation
    def add_permissions(self, create, extracted, **kwargs):
        session = self.session

        klass = PermissionFactory
        klass._meta.sqlalchemy_session = session

        if not create:
            return

        if extracted:
            for perm in extracted:
                try:
                    self.permissions.append(perm)
                    session.commit()

                except IntegrityError:
                    session.rollback()
