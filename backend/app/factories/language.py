import factory

from app.database import SessionLocal
from app.models import Language


class LanguageFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        sqlalchemy_session = SessionLocal
        sqlalchemy_session_persistence = "commit"
        sqlalchemy_get_or_create = ("name", "level")
        model = Language

    level = factory.Faker(
        "random_element",
        elements=["A1", "A2", "B1", "B2", "C1", "C2"],
    )

    name = factory.Faker(
        "random_element",
        elements=[
            "English",
            "Polish",
            "Russian",
            "French",
            "Spanish",
            "Italian",
            "Korean",
        ],
    )
