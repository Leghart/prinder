from datetime import datetime, timedelta
from random import randint

import factory
from faker import Faker

from app.database import SessionLocal
from app.models import Project

from .language import LanguageFactory
from .technology import TechnologyFactory
from .user import UserFactory

fake = Faker()
seq_num = 0


def random_date():
    start_date = datetime(2012, 1, 1)
    end_date = datetime.now()
    days = (end_date - start_date).days
    return start_date + timedelta(days=randint(0, days))


class ProjectFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        sqlalchemy_session = SessionLocal
        sqlalchemy_session_persistence = "commit"
        model = Project

    url = factory.Faker("hostname")
    archived = factory.Faker("random_element", elements=[True, False])
    start_date = factory.LazyFunction(random_date)
    end_date = factory.LazyAttribute(lambda obj: obj.start_date + timedelta(days=randint(1, 700)))

    @classmethod
    def _create(cls, target_class, *args, **kwargs):
        global seq_num
        seq_num += 1
        return super()._create(target_class, *args, **kwargs)

    @factory.lazy_attribute
    def name(self):
        return f"{fake.company()[:27]}{seq_num}"

    @factory.lazy_attribute
    def description(self):
        if words := randint(10, 100) <= 80:
            return fake.paragraph(nb_sentences=words)

    @factory.post_generation
    def owner(self, create, extracted, **kwargs):
        session = self.session

        klass = UserFactory
        klass._meta.sqlalchemy_session = session

        if not create:
            return

        if extracted:
            self.owner = extracted
        else:
            self.owner = klass()

        session.commit()

    @factory.post_generation
    def liked_by(self, create, extracted, **kwargs):
        session = self.session

        klass = UserFactory
        klass._meta.sqlalchemy_session = session

        if not create:
            return

        if extracted:
            self.liked_by = extracted

        session.commit()

    @factory.post_generation
    def language(self, create, extracted, **kwargs):
        session = self.session

        klass = LanguageFactory
        klass._meta.sqlalchemy_session = session

        if not create:
            return

        if extracted:
            self.language = extracted
        else:
            self.language = klass()

        session.commit()

    @factory.post_generation
    def add_collaborators(self, create, extracted, **kwargs):
        session = self.session

        klass = UserFactory
        klass._meta.sqlalchemy_session = session

        if not create:
            return

        if extracted:
            for user in extracted:
                if user is not self.owner:
                    self.collaborators.append(user)

        session.commit()

    @factory.post_generation
    def add_technologies(self, create, extracted, **kwargs):
        session = self.session

        klass = TechnologyFactory
        klass._meta.sqlalchemy_session = session

        if not create:
            return

        if extracted:
            for tech in extracted:
                self.technologies.append(tech)

        session.commit()
