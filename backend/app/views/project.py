from typing import Annotated

from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session

from app.actions.project import ProjectActions
from app.database import get_db
from app.exceptions import AlreadyExistsError
from app.schemas.exceptions import BadRequestSchema, ConflictSchema, NotFoundSchema
from app.schemas.project import ProjectCreateSchema, ProjectReadSchema, ProjectUpdateSchema
from app.schemas.technology import TechnologyBase, TechnologyIDSchema
from app.schemas.user import UserIDSchema, UserReadSchema
from app.utils.auth import authentication

router = APIRouter(tags=["Project"])


@router.post(
    "/",
    response_model=ProjectReadSchema,
    status_code=status.HTTP_201_CREATED,
    responses={409: {"model": ConflictSchema}, 400: {"model": BadRequestSchema}},
)
def create_project(
    body: ProjectCreateSchema,
    *,
    user: Annotated[UserReadSchema, authentication(["projects:create"])],
    db: Session = Depends(get_db),
):
    """Creates a new project via `POST` method.

    If start_date or end_date has invalid format, raises `400 HTTPException`.

    Required permission: `projects:create`.
    """
    try:
        return ProjectActions.create(data=body, db=db)
    except AlreadyExistsError as e:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail=e.detail)
    except ValueError as e:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(e))


@router.get(
    "/{pk}",
    response_model=ProjectReadSchema,
    status_code=status.HTTP_200_OK,
    responses={404: {"model": NotFoundSchema}},
)
def read_project(
    pk: int,
    user: Annotated[UserReadSchema, authentication(["projects:read"])],
    db: Session = Depends(get_db),
):
    """Reads a requested project via `GET` method.

    Required permission: `projects:read`.
    """
    return ProjectActions.read(id=pk, db=db)


@router.get("/", status_code=status.HTTP_200_OK, response_model=list[ProjectReadSchema])
def list_projects(
    user: Annotated[UserReadSchema, authentication(["projects:list"])],
    db: Session = Depends(get_db),
):
    """Reads all projects via `GET` method.

    Required permission: `projects:list`.
    """
    return ProjectActions.list(db=db)


@router.patch(
    "/{pk}",
    status_code=status.HTTP_200_OK,
    response_model=ProjectReadSchema,
    responses={404: {"model": NotFoundSchema}},
)
def update_project(
    pk: int,
    body: ProjectUpdateSchema,
    *,
    user: Annotated[UserReadSchema, authentication(["projects:update"])],
    db: Session = Depends(get_db),
):
    """Updates project with passed fields in request via `PATCH` method.

    Required permission: `projects:update`.
    """
    data = body.dict(exclude_none=True)
    return ProjectActions.update(id=pk, data=data, db=db)


@router.delete(
    "/{pk}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={404: {"model": NotFoundSchema}},
)
def destroy_project(
    pk: int,
    *,
    user: Annotated[UserReadSchema, authentication(["projects:delete"])],
    db: Session = Depends(get_db),
):
    """Destroys a requested project via `DELETE` method.

    Required permission: `projects:delete`.
    """
    return ProjectActions.delete(id=pk, db=db)


@router.post(
    "/{pk}/collaborators/",
    status_code=status.HTTP_200_OK,
    response_model=list[UserIDSchema],
    responses={404: {"model": NotFoundSchema}},
)
def add_collaborator_to_project(pk: int, user: UserIDSchema, db: Session = Depends(get_db)):
    """Adds collaborator to project via `POST` method.

    If requested project or user does not exist, raises
    `404 HTTPException`.

    Required permission:
    """
    return ProjectActions.add_collaborator(project_id=pk, user_id=user.id, db=db)


@router.delete(
    "/{pk}/collaborators/{user_id}",
    status_code=status.HTTP_200_OK,
    response_model=list[UserIDSchema],
    responses={404: {"model": NotFoundSchema}},
)
def remove_collaborator_from_project(pk: int, user_id: int, db: Session = Depends(get_db)):
    """Removes collaborator from project via `DELETE` method.

    If requested project or user does not exist, raises
    `404 HTTPException`. If user exist but is not a collaborator of this
    project, raises `404 HTTPException`.

    Required permission:
    """
    return ProjectActions.remove_collaborator(project_id=pk, user_id=user_id, db=db)


@router.post(
    "/{pk}/likes",
    status_code=status.HTTP_200_OK,
    response_model=list[UserIDSchema],
    responses={404: {"model": NotFoundSchema}},
)
def add_user_like(pk: int, user: UserIDSchema, db: Session = Depends(get_db)):
    """Adds user who like project to project via `POST` method.

    If requested project or user does not exist, raises
    `404 HTTPException`.

    Required permission:
    """
    return ProjectActions.add_like(project_id=pk, user_id=user.id, db=db)


@router.delete(
    "/{pk}/likes/{user_id}",
    status_code=status.HTTP_200_OK,
    response_model=list[UserIDSchema],
    responses={404: {"model": NotFoundSchema}},
)
def remove_user_like(pk: int, user_id: int, db: Session = Depends(get_db)):
    """Removes likers from project via `DELETE` method.

    If requested project or user does not exist, raises
    `404 HTTPException`. If user exist but did not like this project,
    raises `404 HTTPException`.

    Required permission:
    """
    return ProjectActions.remove_like(project_id=pk, user_id=user_id, db=db)


@router.post(
    "/{pk}/technologies",
    status_code=status.HTTP_200_OK,
    response_model=list[TechnologyIDSchema],
    responses={404: {"model": NotFoundSchema}},
)
def add_technology_to_project(pk: int, technology: TechnologyBase, db: Session = Depends(get_db)):
    """Adds technology to project via `POST` method.

    If technology does not exist, create it first, then add to project.
    If requested project raises `404 HTTPException`.

    Required permission:
    """
    return ProjectActions.add_technology(project_id=pk, data=technology, db=db)


@router.delete(
    "/{pk}/technologies/{technology_id}",
    status_code=status.HTTP_200_OK,
    response_model=list[TechnologyIDSchema],
    responses={404: {"model": NotFoundSchema}},
)
def remove_technology_from_project(pk: int, technology_id: int, db: Session = Depends(get_db)):
    """Removes technology from project via `DELETE` method.

    If requested project, raises `404 HTTPException`.

    Required permission:
    """
    return ProjectActions.remove_technology(project_id=pk, technology_id=technology_id, db=db)
