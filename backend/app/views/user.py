from typing import Annotated

from fastapi import APIRouter, Depends, File, HTTPException, UploadFile, status
from fastapi.responses import FileResponse
from sqlalchemy.orm import Session

from app.actions.user import UserActions
from app.database import get_db
from app.exceptions import AlreadyExistsError
from app.schemas.exceptions import BadRequestSchema, ConflictSchema, NotAcceptableSchema, NotFoundSchema
from app.schemas.language import LanguageIDSchema, LanguageReadSchema
from app.schemas.skill import SkillBase, SkillReadSchema
from app.schemas.user import CreateHashedSchema, UpdateHashedSchema, UserCreateSchema, UserReadSchema, UserUpdateSchema
from app.utils.auth import authentication, encrypt_password

router = APIRouter(tags=["User"])


@router.post(
    "/",
    response_model=UserReadSchema,
    status_code=status.HTTP_201_CREATED,
    responses={409: {"model": ConflictSchema}},
)
def create_user(
    body: UserCreateSchema,
    *,
    user: Annotated[UserReadSchema, authentication(["users:create"])],
    db: Session = Depends(get_db),
):
    """Creates a new user via `POST` method.

    Hashes a password before storing it in database.

    Required permission: `users:create`.
    """
    encrypted_user = CreateHashedSchema(
        first_name=body.first_name,
        last_name=body.last_name,
        email=body.email,
        username=body.username,
        password=encrypt_password(body.password),
        bio=body.bio,
    )
    try:
        return UserActions.create(data=encrypted_user, db=db)
    except AlreadyExistsError as e:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail=e.detail)


@router.get(
    "/{pk}",
    response_model=UserReadSchema,
    status_code=status.HTTP_200_OK,
    responses={404: {"model": NotFoundSchema}},
)
def read_user(
    pk: int,
    user: Annotated[UserReadSchema, authentication(["users:read"])],
    db: Session = Depends(get_db),
):
    """Reads a requested user via `GET` method.

    Required permission: `users:read`.
    """
    return UserActions.read(id=pk, db=db)


@router.get(
    "/",
    response_model=list[UserReadSchema],
    status_code=status.HTTP_200_OK,
)
def list_users(
    user: Annotated[UserReadSchema, authentication(["users:list"])],
    db: Session = Depends(get_db),
):
    """Reads all users via `GET` method.

    Required permission: `users:list`.
    """
    return UserActions.list(db=db)


@router.patch(
    "/{pk}",
    status_code=status.HTTP_200_OK,
    response_model=UserReadSchema,
    responses={404: {"model": NotFoundSchema}},
)
def update_user(
    pk: int,
    body: UserUpdateSchema,
    *,
    user: Annotated[UserReadSchema, authentication(["users:update"])],
    db: Session = Depends(get_db),
):
    """Updates user with passed fields in request via `PATCH` method.

    If password was in request, hashes it before save to database.

    Required permission: `users:update`.
    """
    user_data: UserUpdateSchema | UpdateHashedSchema
    if body.password:
        encrypted_user = UpdateHashedSchema(
            first_name=body.first_name,
            last_name=body.last_name,
            email=body.email,
            username=body.username,
            password=encrypt_password(body.password),
            bio=body.bio,
        )
        user_data = encrypted_user

    else:
        user_data = body

    data = user_data.dict(exclude_none=True)
    return UserActions.update(id=pk, data=data, db=db)


@router.delete(
    "/{pk}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={404: {"model": NotFoundSchema}},
)
def destroy_user(
    pk: int,
    *,
    user: Annotated[UserReadSchema, authentication(["users:delete"])],
    db: Session = Depends(get_db),
):
    """Destroys a requested user via `DELETE` method.

    Required permission: `users:delete`.
    """
    return UserActions.delete(id=pk, db=db)


@router.post(
    "/{pk}/image/",
    status_code=status.HTTP_200_OK,
    response_model=UserReadSchema,
    responses={
        404: {"model": NotFoundSchema},
        406: {"model": NotAcceptableSchema},
    },
)
async def add_image_to_user(pk: int, image: UploadFile = File(...), db: Session = Depends(get_db)):
    """Adds image to user via `POST` method.

    Function is asynchronous, to allow time for image processing.
    If user already had an image, the old one will be removed.

    Required permission:
    """
    return await UserActions.add_image(user_id=pk, image=image, db=db)


@router.get(
    "/{pk}/image/",
    status_code=status.HTTP_200_OK,
    responses={404: {"model": NotFoundSchema}},
)
async def get_user_image(pk: int, db: Session = Depends(get_db)):
    """Gets image path from user via `GET` method.

    Function is asynchronous, to allow time for image processing.
    If requested user does not exist or has not an image, raises
    `404 HTTPException`.

    Required permission:
    """
    if not (path := UserActions.read(pk, db=db).image):
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="The user has no image.")

    return FileResponse(path)


@router.delete(
    "/{pk}/image/",
    status_code=status.HTTP_200_OK,
    response_model=UserReadSchema,
    responses={404: {"model": NotFoundSchema}},
)
def remove_user_image(pk: int, db: Session = Depends(get_db)):
    """Removes image from user via `DELETE` method.

    If requested user does not exist, raises `404 HTTPException`.
    If requested user has not an image, do nothing.

    Required permission:
    """
    return UserActions.remove_image(user_id=pk, db=db)


@router.post(
    "/{pk}/skills",
    status_code=status.HTTP_200_OK,
    response_model=list[SkillReadSchema],
    responses={
        404: {"model": NotFoundSchema},
        400: {"model": BadRequestSchema},
    },
)
def add_skill_to_user(pk: int, skills_data: SkillBase, db: Session = Depends(get_db)):
    """Adds skill to user via `POST` method.

    If requested user does not exist, raises `404 HTTPException`.
    If user already has a requested skill (by name), raises
    `400 HTTPException`.

    Required permission:
    """
    return UserActions.add_skill(user_id=pk, data=skills_data, db=db)


@router.delete(
    "/{pk}/skills/{skill_id}",
    status_code=status.HTTP_200_OK,
    response_model=list[SkillReadSchema],
    responses={404: {"model": NotFoundSchema}},
)
def remove_skill_from_user(pk: int, skill_id: int, db: Session = Depends(get_db)):
    """Removes skill from user via `DELETE` method.

    If requested user does not exist, raises `404 HTTPException`.
    If requested skill does not exist, raises `404 HTTPException`.

    Required permission:
    """
    return UserActions.remove_skill(user_id=pk, skill_id=skill_id, db=db)


@router.post(
    "/{pk}/languages",
    status_code=status.HTTP_200_OK,
    response_model=list[LanguageReadSchema],
    responses={
        404: {"model": NotFoundSchema},
        400: {"model": BadRequestSchema},
    },
)
def add_language_to_user(pk: int, language: LanguageIDSchema, db: Session = Depends(get_db)):
    """Adds language to user via `POST` method.

    If requested user does not exist, raises `404 HTTPException`.
    If user already has a requested language (by name), raises
    `400 HTTPException`.

    Required permission:
    """
    return UserActions.add_language(user_id=pk, language_id=language.id, db=db)


@router.delete(
    "/{pk}/languages/{language_id}",
    status_code=status.HTTP_200_OK,
    response_model=list[LanguageReadSchema],
    responses={404: {"model": NotFoundSchema}},
)
def remove_language_from_user(pk: int, language_id: int, db: Session = Depends(get_db)):
    """Removes language from user via `DELETE` method.

    If requested user does not exist, raises `404 HTTPException`.
    If requested language does not exist, raises `404 HTTPException`.
    If requested language exist, but user has not this language, raises
    `404 HTTPException`.

    Required permission:
    """
    return UserActions.remove_language(user_id=pk, language_id=language_id, db=db)
