from typing import Annotated

from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session

from app.actions.technology import TechnologyActions
from app.database import get_db
from app.exceptions import AlreadyExistsError
from app.schemas.exceptions import ConflictSchema, NotFoundSchema
from app.schemas.technology import TechnologyCreateSchema, TechnologyReadSchema, TechnologyUpdateSchema
from app.schemas.user import UserReadSchema
from app.utils.auth import authentication

router = APIRouter(tags=["Technology"])


@router.post(
    "/",
    status_code=status.HTTP_201_CREATED,
    response_model=TechnologyReadSchema,
    responses={409: {"model": ConflictSchema}},
)
def create_technology(
    body: TechnologyCreateSchema,
    *,
    user: Annotated[UserReadSchema, authentication(["technologies:create"])],
    db: Session = Depends(get_db),
):
    """Creates a new technology via `POST` method.

    Required permission: `technologies:create`.
    """
    try:
        return TechnologyActions.create(data=body, db=db)
    except AlreadyExistsError as e:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail=e.detail)


@router.get(
    "/{pk}",
    response_model=TechnologyReadSchema,
    status_code=status.HTTP_200_OK,
    responses={404: {"model": NotFoundSchema}},
)
def read_technology(
    pk: int,
    user: Annotated[UserReadSchema, authentication(["technologies:read"])],
    db: Session = Depends(get_db),
):
    """Reads a requested technology via `GET` method.

    Required permission: `technologies:read`.
    """
    return TechnologyActions.read(id=pk, db=db)


@router.get(
    "/",
    response_model=list[TechnologyReadSchema],
    status_code=status.HTTP_200_OK,
)
def list_technologies(
    user: Annotated[UserReadSchema, authentication(["technologies:list"])],
    db: Session = Depends(get_db),
):
    """Reads all technologies via `GET` method.

    Required permission: `technologies:list`.
    """
    return TechnologyActions.list(db=db)


@router.patch(
    "/{pk}",
    status_code=status.HTTP_200_OK,
    response_model=TechnologyReadSchema,
    responses={404: {"model": NotFoundSchema}},
)
def update_technology(
    pk: int,
    body: TechnologyUpdateSchema,
    *,
    user: Annotated[UserReadSchema, authentication(["technologies:update"])],
    db: Session = Depends(get_db),
):
    """Updates technology with passed fields in request via `PATCH` method.

    Required permission: `technologies:update`.
    """
    data = body.dict(exclude_none=True)
    return TechnologyActions.update(id=pk, data=data, db=db)


@router.delete(
    "/{pk}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={404: {"model": NotFoundSchema}},
)
def destroy_technology(
    pk: int,
    *,
    user: Annotated[UserReadSchema, authentication(["technologies:delete"])],
    db: Session = Depends(get_db),
):
    """Destroys a requested technology via `DELETE` method.

    Required permission: `technologies:delete`.
    """
    return TechnologyActions.delete(id=pk, db=db)
