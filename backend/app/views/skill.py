from typing import Annotated

from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session

from app.actions.skill import SkillActions
from app.database import get_db
from app.exceptions import AlreadyExistsError
from app.schemas.exceptions import ConflictSchema, NotFoundSchema
from app.schemas.skill import SkillCreateSchema, SkillReadSchema, SkillUpdateSchema
from app.schemas.user import UserReadSchema
from app.utils.auth import authentication

router = APIRouter(tags=["Skill"])


@router.post(
    "/",
    response_model=SkillReadSchema,
    status_code=status.HTTP_201_CREATED,
    responses={409: {"model": ConflictSchema}},
)
def create_skill(
    body: SkillCreateSchema,
    *,
    user: Annotated[UserReadSchema, authentication(["skills:create"])],
    db: Session = Depends(get_db),
):
    """Creates a new skill via `POST` method.

    Required permission: `skills:create`.
    """
    try:
        return SkillActions.create(data=body, db=db)
    except AlreadyExistsError as e:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail=e.detail)


@router.get(
    "/{pk}",
    response_model=SkillReadSchema,
    status_code=status.HTTP_200_OK,
    responses={404: {"model": NotFoundSchema}},
)
def read_skill(
    pk: int,
    user: Annotated[UserReadSchema, authentication(["skills:read"])],
    db: Session = Depends(get_db),
):
    """Reads a requested skill via `GET` method.

    Required permission: `skills:read`.
    """
    return SkillActions.read(id=pk, db=db)


@router.get("/", status_code=status.HTTP_200_OK, response_model=list[SkillReadSchema])
def list_skills(
    user: Annotated[UserReadSchema, authentication(["skills:list"])],
    db: Session = Depends(get_db),
):
    """Reads all skills via `GET` method.

    Required permission: `skills:list`.
    """
    return SkillActions.list(db=db)


@router.patch(
    "/{pk}",
    status_code=status.HTTP_200_OK,
    response_model=SkillReadSchema,
    responses={404: {"model": NotFoundSchema}},
)
def update_skill(
    pk: int,
    body: SkillUpdateSchema,
    *,
    user: Annotated[UserReadSchema, authentication(["skills:update"])],
    db: Session = Depends(get_db),
):
    """Updates skill with passed fields in request via `PATCH` method.

    Required permission: `skills:update`.
    """
    data = body.dict(exclude_none=True)
    return SkillActions.update(id=pk, data=data, db=db)


@router.delete(
    "/{pk}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={404: {"model": NotFoundSchema}},
)
def destroy_skill(
    pk: int,
    *,
    user: Annotated[UserReadSchema, authentication(["skills:delete"])],
    db: Session = Depends(get_db),
):
    """Destroys a requested skill via `DELETE` method.

    Required permission: `skills:delete`.
    """
    return SkillActions.delete(id=pk, db=db)
