from datetime import timedelta
from typing import Annotated

from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from app.database import get_db
from app.schemas.exceptions import UnauthorizedSchema
from app.schemas.token import TokenSchema
from app.settings import settings
from app.utils.auth import authenticate_user, create_access_token

router = APIRouter()


@router.post(
    "/token/",
    status_code=status.HTTP_200_OK,
    response_model=TokenSchema,
    responses={401: {"model": UnauthorizedSchema}},
)
async def get_access_token(form_data: Annotated[OAuth2PasswordRequestForm, Depends()], db: Session = Depends(get_db)):
    """Generates access token for user.

    Token is created from passed form data with fields: `username` and
    `password`. If credentials are invalid or user does not exist, raise
    `HTTPException`.
    """
    user_jwt = authenticate_user(form_data.username, form_data.password, db=db)

    if not user_jwt:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={
            "username": user_jwt.username,
            "scopes": [f"{p.table}:{p.name}" for p in user_jwt.permissions],
            "is_superuser": user_jwt.is_superuser,
            "id": user_jwt.id,
        },
        expires_delta=access_token_expires,
    )
    return {"access_token": access_token, "token_type": "bearer"}
