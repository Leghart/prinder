from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session

from app.actions.permission import PermissionActions
from app.actions.user import UserActions
from app.schemas.exceptions import ConflictSchema, NotFoundSchema
from app.schemas.permission import PermissionCreateSchema, PermissionReadSchema
from app.schemas.user import UserPermissionsSchema, UserReadSchema

from ..database import get_db
from ..exceptions import AlreadyExistsError

router = APIRouter()


@router.post(
    "/permissions/",
    status_code=status.HTTP_201_CREATED,
    response_model=PermissionReadSchema,
    responses={409: {"model": ConflictSchema}},
)
def create_permission(permission: PermissionCreateSchema, *, db: Session = Depends(get_db)):
    """Creates a new permission via `POST` method."""
    try:
        return PermissionActions.create(data=permission, db=db)
    except AlreadyExistsError as e:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail=e.detail)


@router.get(
    "/permissions/{pk}",
    response_model=PermissionReadSchema,
    responses={404: {"model": NotFoundSchema}},
)
def read_permission(pk: int, db: Session = Depends(get_db)):
    """Reads a requested permission via `GET` method."""
    return PermissionActions.read(id=pk, db=db)


@router.get("/permissions/", status_code=status.HTTP_200_OK, response_model=list[PermissionReadSchema])
def list_permissions(db: Session = Depends(get_db)):
    """Reads all permissions via `GET` method."""
    return PermissionActions.list(db=db)


@router.delete(
    "/permissions/{pk}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={404: {"model": NotFoundSchema}},
)
def destroy_permission(pk: int, db: Session = Depends(get_db)):
    """Destroys a requested permission via `DELETE` method."""
    return PermissionActions.delete(id=pk, db=db)


@router.post(
    "/users/{user_pk}/permissions/{permission_pk}",
    status_code=status.HTTP_200_OK,
    response_model=UserPermissionsSchema,
    responses={404: {"model": NotFoundSchema}},
)
def add_permission_to_user(user_pk: int, permission_pk: int, db: Session = Depends(get_db)):
    """Adds a permission to user via `POST` method.

    If user already has a requested permission, do nothing.

    If requested user does not exist, raises `404 HTTPException`.
    If requested permission does not exist, raises `404 HTTPException`.
    """
    return PermissionActions.add_user(user_id=user_pk, permission_id=permission_pk, db=db)


@router.delete(
    "/users/{user_pk}/permissions/{permission_pk}",
    status_code=status.HTTP_200_OK,
    response_model=UserReadSchema,
    responses={404: {"model": NotFoundSchema}},
)
def remove_permission_from_user(user_pk: int, permission_pk: int, db: Session = Depends(get_db)):
    """Removes a permission from user via `DELETE` method.

    If user has not a requested permission, do nothing.

    If requested user does not exist, raises `404 HTTPException`.
    If requested permission does not exist, raises `404 HTTPException`.
    """
    return PermissionActions.remove_user(user_id=user_pk, permission_id=permission_pk, db=db)


@router.get(
    "/users/{user_pk}/permissions/",
    status_code=status.HTTP_200_OK,
    response_model=UserPermissionsSchema,
    responses={404: {"model": NotFoundSchema}},
)
def read_user_permissions(user_pk: int, db: Session = Depends(get_db)):
    """Reads all permissions of user via `GET` method.

    If requested user does not exist, raises `404 HTTPException`.
    """
    return UserActions.read(id=user_pk, db=db)
