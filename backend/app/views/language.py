from typing import Annotated

from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session

from app.actions.language import LanguageActions
from app.database import get_db
from app.exceptions import AlreadyExistsError
from app.schemas.exceptions import ConflictSchema, NotFoundSchema
from app.schemas.language import LanguageCreateSchema, LanguageReadSchema, LanguageUpdateSchema
from app.schemas.user import UserReadSchema
from app.utils.auth import authentication

router = APIRouter(tags=["Language"])


@router.post(
    "/",
    response_model=LanguageReadSchema,
    status_code=status.HTTP_201_CREATED,
    responses={409: {"model": ConflictSchema}},
)
def create_language(
    body: LanguageCreateSchema,
    *,
    user: Annotated[UserReadSchema, authentication(["languages:create"])],
    db: Session = Depends(get_db),
):
    """Creates a new language via `POST` method.

    Required permission: `languages:create`.
    """
    try:
        return LanguageActions.create(data=body, db=db)
    except AlreadyExistsError as e:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail=e.detail)


@router.get(
    "/{pk}",
    response_model=LanguageReadSchema,
    status_code=status.HTTP_200_OK,
    responses={404: {"model": NotFoundSchema}},
)
def read_language(
    pk: int,
    user: Annotated[UserReadSchema, authentication(["languages:read"])],
    db: Session = Depends(get_db),
):
    """Reads a requested language via `GET` method.

    Required permission: `languages:read`.
    """
    return LanguageActions.read(id=pk, db=db)


@router.get("/", response_model=list[LanguageReadSchema], status_code=status.HTTP_200_OK)
def list_languages(
    user: Annotated[UserReadSchema, authentication(["languages:list"])],
    db: Session = Depends(get_db),
):
    """Reads all languages via `GET` method.

    Required permission: `languages:list`.
    """
    return LanguageActions.list(db=db)


@router.patch(
    "/{pk}",
    status_code=status.HTTP_200_OK,
    response_model=LanguageReadSchema,
    responses={404: {"model": NotFoundSchema}},
)
def update_language(
    pk: int,
    body: LanguageUpdateSchema,
    *,
    user: Annotated[UserReadSchema, authentication(["languages:update"])],
    db: Session = Depends(get_db),
):
    """Updates language with passed fields in request via `PATCH` method.

    Required permission: `languages:update`.
    """
    data = body.dict(exclude_none=True)
    return LanguageActions.update(id=pk, data=data, db=db)


@router.delete("/{pk}", status_code=status.HTTP_204_NO_CONTENT, responses={404: {"model": NotFoundSchema}})
def destroy_language(
    pk: int,
    *,
    user: Annotated[UserReadSchema, authentication(["languages:delete"])],
    db: Session = Depends(get_db),
):
    """Destroys a requested language via `DELETE` method.

    Required permission: `languages:delete`.
    """
    return LanguageActions.delete(id=pk, db=db)
