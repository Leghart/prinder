from fastapi import APIRouter

from .views import admin, auth, language, project, skill, technology, user

router = APIRouter()
router.include_router(user.router, prefix="/users")
router.include_router(language.router, prefix="/languages")
router.include_router(skill.router, prefix="/skills")
router.include_router(project.router, prefix="/projects")
router.include_router(technology.router, prefix="/technologies")
router.include_router(admin.router, prefix="/admin", tags=["Admin"])
router.include_router(auth.router, tags=["Authentication"])
