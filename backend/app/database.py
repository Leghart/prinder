from fastapi import Request
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker

from .settings import settings

engine = create_engine(settings.DATABASE_URL)
Base = declarative_base()

SessionLocal = scoped_session(sessionmaker(autoflush=False, bind=engine))


engine_test = create_engine(settings.DATABASE_URL_TEST)

TestingSessionLocal = scoped_session(sessionmaker(bind=engine_test))


def get_db(request: Request):
    """Return db instance from request state."""
    return request.state.db


def get_session():
    """Return session generator.

    Can be used to manually access to database.
    Has to be called via next(get_session()).
    """
    try:
        session = SessionLocal()
        yield session
    finally:
        session.close()


def init_db(engine=engine) -> None:
    """Creates tables if doesn't exist."""
    Base.metadata.create_all(engine)


def clear_db(engine=engine) -> None:
    Base.metadata.drop_all(engine)


def get_all_tables() -> list[str]:
    return Base.metadata.tables.keys()


def get_main_tables() -> list[str]:
    return [table for table in Base.metadata.tables.keys() if len(table.split("_")) == 1]
