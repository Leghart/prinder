from ..models import Skill
from .base import ActionsBase


class SkillActions(ActionsBase):
    _model = Skill
