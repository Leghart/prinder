from ..models import Language
from .base import ActionsBase


class LanguageActions(ActionsBase):
    _model = Language
