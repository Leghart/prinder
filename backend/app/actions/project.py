from fastapi import HTTPException, status
from sqlalchemy.orm import Session

from ..actions.technology import TechnologyActions
from ..actions.user import UserActions
from ..models import Project
from ..schemas.project import ProjectCreateSchema
from ..schemas.technology import TechnologyBase, TechnologyCreateSchema, TechnologyReadSchema
from .base import ActionsBase


class ProjectActions(ActionsBase):
    _model = Project

    @classmethod
    def create(cls, data: ProjectCreateSchema, db: Session):
        start_date = cls._model.parse_date(data.start_date)
        end_date = cls._model.parse_date(data.end_date)

        owner = UserActions.get(id=data.owner_id, db=db)

        new_data = data.dict() | {"owner": owner, "start_date": start_date, "end_date": end_date}
        return super().create(new_data, db)

    @classmethod
    def add_collaborator(cls, project_id: int, user_id: int, db: Session):
        project = ProjectActions.read(id=project_id, db=db)
        user = UserActions.read(id=user_id, db=db)

        if user not in project.collaborators:
            project.collaborators.append(user)

            db.add(project)
            db.commit()
            db.refresh(project)

        return project.collaborators

    @classmethod
    def remove_collaborator(cls, project_id: int, user_id: int, db: Session):
        project = ProjectActions.read(id=project_id, db=db)
        user = UserActions.read(id=user_id, db=db)

        if user not in project.collaborators:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="Passed user is not collaborator of this project."
            )

        project.collaborators.remove(user)

        db.add(project)
        db.commit()
        db.refresh(project)

        return project.collaborators

    @classmethod
    def add_like(cls, project_id: int, user_id: int, db: Session):
        project = ProjectActions.read(id=project_id, db=db)
        user = UserActions.read(id=user_id, db=db)

        if user not in project.liked_by:
            project.liked_by.append(user)

            db.add(project)
            db.commit()
            db.refresh(project)

        return project.liked_by

    @classmethod
    def remove_like(cls, project_id: int, user_id: int, db: Session):
        project = ProjectActions.read(id=project_id, db=db)
        user = UserActions.read(id=user_id, db=db)

        if user not in project.liked_by:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Passed user didn't like this project.")

        project.liked_by.remove(user)

        db.add(project)
        db.commit()
        db.refresh(project)

        return project.liked_by

    @classmethod
    def add_technology(cls, project_id: int, data: TechnologyBase, db: Session) -> list[TechnologyReadSchema]:
        """Allows to add technology to project instance.

        If passed `project_id` refers object which doesn't exist, method raises `HTTPException`.
        If technology object (with the same data as passed to method) exists, then we get it instead
        of create duplicate.

        Returns the current technologies list.
        """
        project = ProjectActions.read(id=project_id, db=db)
        technology_data = TechnologyCreateSchema(name=data.name, version=data.version, project_id=project.id)
        technology = TechnologyActions.get_or_create(**technology_data.dict(), db=db)

        if technology not in project.technologies:
            project.technologies.append(technology)
            db.commit()
            db.refresh(project)

        return project.technologies

    @classmethod
    def remove_technology(cls, project_id: int, technology_id: int, db: Session) -> list[TechnologyReadSchema]:
        """Allows to remove technology from project intance.

        If passed `project_id` refers object which doesn't exist, method raises HTTPException.

        Returns the current technologies list (after removing).
        """
        project = ProjectActions.read(id=project_id, db=db)
        TechnologyActions.delete(id=technology_id, db=db)

        db.commit()
        db.refresh(project)

        return project.technologies
