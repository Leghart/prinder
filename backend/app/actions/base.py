from abc import ABC
from datetime import datetime
from typing import Type

import pydantic
from fastapi import HTTPException, status
from sqlalchemy import exc
from sqlalchemy.orm import Session

from ..database import Base
from ..exceptions import AlreadyExistsError


class CrudBase(ABC):
    _not_found_msg: str = "Object does not exist ({}, id={})."
    _model: Type[Base]

    @classmethod
    def create(cls, data, db: Session):
        """Create object in database.

        Deserializes data into pydantic model to validate it and
        adds to database. If there will be an integrity error (on database layer),
        then rollback transaction and raises `AlreadyExistsError` up.

        If save into db was succesful, returns object (pydantic instance).
        """
        if isinstance(data, pydantic.BaseModel):
            object = cls._model(**data.dict())
        else:
            object = cls._model(**data)

        try:
            db.add(object)
            db.commit()
            return object
        except exc.IntegrityError as err:
            db.rollback()
            if err.orig:
                raise AlreadyExistsError(err.orig.args[0])
            else:
                raise

    @classmethod
    def read(cls, id: int, db: Session):
        """Return object by id.

        If object does not exist, raise `HTTPException` with code 404.
        """
        if not (obj := db.get(cls._model, id)):
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=cls._not_found_msg.format(cls._model.__name__, id),
            )

        return obj

    @classmethod
    def list(cls, db: Session):
        """Get list containing all objects.

        If there is not any records, returns empty list.
        """
        return db.query(cls._model).all()

    @classmethod
    def update(cls, id: int, data, db: Session):
        """Update instance by passed data.

        If object does not exist, raise `HTTPException` with code 404,
        otherwise return updated (refreshed) instance.
        """
        if not (obj := db.get(cls._model, id)):
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=cls._not_found_msg.format(cls._model.__name__, id),
            )

        for field, value in data.items():
            setattr(obj, field, value)

        obj.updated_at = datetime.now()

        db.commit()
        db.refresh(obj)
        return obj

    @classmethod
    def delete(cls, id: int, db: Session):
        """Delete object from database.

        If object does not exist, raise `HTTPException` with code 404,
        otherwise return data describes deleted object.
        """
        if not (obj := db.get(cls._model, id)):
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=cls._not_found_msg.format(cls._model.__name__, id),
            )

        db.delete(obj)
        db.commit()
        return obj


class ActionsBase(CrudBase):
    _model: Type[Base]

    @classmethod
    def get(cls, db: Session, **kwargs: str | int):
        """Method to get object from database which fits to passed
        filters. If object does not exist, return None"""
        return db.query(cls._model).filter_by(**kwargs).first()

    @classmethod
    def get_or_create(cls, db: Session, **kwargs: str | int):
        """Method to get object with passed filters from database, or
        create it if does not exist."""
        if obj := cls.get(**kwargs, db=db):
            return obj

        return cls.create(kwargs, db)
