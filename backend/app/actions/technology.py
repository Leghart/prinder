from ..models import Technology
from .base import ActionsBase


class TechnologyActions(ActionsBase):
    _model = Technology
