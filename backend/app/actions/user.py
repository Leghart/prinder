from fastapi import HTTPException, UploadFile, status
from sqlalchemy.orm import Session

from app.utils.file import delete_file, handle_file_upload

from ..actions.language import LanguageActions
from ..actions.skill import SkillActions
from ..models import User
from ..schemas.language import LanguageReadSchema
from ..schemas.skill import SkillBase, SkillCreateSchema, SkillReadSchema
from ..schemas.user import UserReadSchema
from .base import ActionsBase


class UserActions(ActionsBase):
    _model = User

    @classmethod
    def add_skill(cls, user_id: int, data: SkillBase, db: Session) -> list[SkillReadSchema]:
        """Allows to add skill to user instance.

        If passed `user_id` refers object which doesn't exist, method raises `HTTPException`.
        If skill object (with the same data as passed to method) exists, then we get it instead
        of create duplicate.

        Returns the current skills list.
        """
        user = UserActions.read(id=user_id, db=db)

        if data.name in [s.name for s in user.skills]:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"User already has '{data.name}' skill. Change level instead of creating a new one.",
            )

        skill_data = SkillCreateSchema(name=data.name, level=data.level, user_id=user_id)
        skill = SkillActions.get_or_create(**skill_data.dict(), db=db)

        user.skills.append(skill)
        db.commit()
        db.refresh(user)

        return user.skills

    @classmethod
    def remove_skill(cls, user_id: int, skill_id: int, db: Session) -> list[SkillReadSchema]:
        """Allows to remove skill from user intance.

        If passed `user_id` refers object which doesn't exist, method raises HTTPException.

        Returns the current skills list (after removing).
        """
        user = UserActions.read(id=user_id, db=db)
        SkillActions.delete(id=skill_id, db=db)

        db.commit()
        db.refresh(user)

        return user.skills

    @classmethod
    def add_language(cls, user_id: int, language_id: int, db: Session) -> list[LanguageReadSchema]:
        """Allows to add language to user intance.

        If passed data to find objects is incorrect (objects with that
        ids don't exist), then raise `HTTPException`.
        Otherwise returns the current languages list.
        """
        user = UserActions.read(id=user_id, db=db)
        language = LanguageActions.read(id=language_id, db=db)

        if language.name in [s.name for s in user.languages]:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"User already has '{language.name}' language. Change level instead of creating a new one.",
            )

        user.languages.append(language)
        db.commit()
        db.refresh(user)

        return user.languages

    @classmethod
    def remove_language(cls, user_id: int, language_id: int, db: Session) -> list[LanguageReadSchema]:
        """Allows to remove language from user intance.

        If passed data to find objects is incorrect (objects with that
        ids don't exist), then raise `HTTPException`. Additionaly method is
        checking whether passed `language_id` refers to object which is
        connected with this user. If not, raises `HTTPException`.

        Returns the current languages list (after removing).
        """
        user = UserActions.read(id=user_id, db=db)
        language = LanguageActions.read(id=language_id, db=db)

        if language not in user.languages:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail=f"User has not language with id={language_id}"
            )

        user.languages.remove(language)

        db.commit()
        db.refresh(user)

        return user.languages

    @classmethod
    async def add_image(cls, user_id: int, image: UploadFile, db: Session) -> UserReadSchema:
        """Allows to add image to user instance.

        If passed `user_id` refers object which doesn't exist, method raises `HTTPException`.

        Returns the updated user with saved image.
        """
        user = UserActions.read(id=user_id, db=db)

        if user.image:
            delete_file(user.image)

        img_path = await handle_file_upload(image)

        user.image = img_path

        db.commit()
        db.refresh(user)

        return user

    @classmethod
    def remove_image(cls, user_id: int, db: Session) -> UserReadSchema:
        """Allows to remove image from user instance.

        If passed `user_id` refers object which doesn't exist, method raises `HTTPException`.

        Returns the updated user without image.
        """
        user = UserActions.read(id=user_id, db=db)

        if user.image:
            delete_file(user.image)
            user.image = None

        db.commit()
        db.refresh(user)

        return user
