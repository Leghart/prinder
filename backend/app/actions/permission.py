from sqlalchemy.orm import Session

from app.schemas.user import UserPermissionsSchema

from ..models import Permission
from .base import ActionsBase
from .user import UserActions


class PermissionActions(ActionsBase):
    _model = Permission

    @classmethod
    def add_user(cls, user_id: int, permission_id: int, db: Session) -> UserPermissionsSchema:
        """Allows to add permission to user (both by ids)."""
        permission = PermissionActions.read(id=permission_id, db=db)
        user = UserActions.read(id=user_id, db=db)

        if permission not in user.permissions:
            user.permissions.append(permission)
            db.commit()
            db.refresh(user)

        return user

    @classmethod
    def remove_user(cls, user_id: int, permission_id: int, db: Session) -> UserPermissionsSchema:
        """Allows to remove permission from user (both by ids)."""
        permission = PermissionActions.read(id=permission_id, db=db)
        user = UserActions.read(id=user_id, db=db)

        if permission in user.permissions:
            user.permissions.remove(permission)
            db.commit()
            db.refresh(user)

        return user
