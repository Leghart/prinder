import os
from typing import Optional

from pydantic import BaseSettings


class Settings(BaseSettings):  # type: ignore
    PROJET_NAME: str = "Prinder"

    POSTGRES_PASSWORD: Optional[str] = os.getenv("POSTGRES_PASSWORD")
    POSTGRES_SERVER: str = os.getenv("POSTGRES_SERVER", "localhost")
    POSTGRES_PORT: str = os.getenv("POSTGRES_PORT", "5432")
    POSTGRES_DB_PROD: str = os.getenv("POSTGRES_DB_PROD", "prod_db")
    POSTGRES_DB_TEST: str = os.getenv("POSTGRES_DB_TEST", "test_db")

    DATABASE_URL: str = (
        f"postgresql://{POSTGRES_DB_PROD}:{POSTGRES_PASSWORD}@" f"{POSTGRES_SERVER}:{POSTGRES_PORT}/{POSTGRES_DB_PROD}"
    )
    DATABASE_URL_TEST: str = (
        f"postgresql://{POSTGRES_DB_TEST}:{POSTGRES_PASSWORD}@" f"{POSTGRES_SERVER}:{POSTGRES_PORT}/{POSTGRES_DB_TEST}"
    )

    AUTH: str = os.getenv("AUTH", "True")
    STATIC_FILES_PATH: str = "/app/frontend/public"

    SECRET_KEY: Optional[str] = os.getenv("SECRET_KEY", "test_secret_key")
    ALGORITHM: Optional[str] = os.getenv("ALGORITHM", "HS256")
    ACCESS_TOKEN_EXPIRE_MINUTES = 30

    ORIGINS: list[str] = ["http://localhost", "https://localhost", "http://localhost:8080", "http://127.0.0.1:8080"]

    class Config:
        case_sensitive = True


settings = Settings()
