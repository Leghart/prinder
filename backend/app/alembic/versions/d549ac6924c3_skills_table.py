"""skills table

Revision ID: d549ac6924c3
Revises: 13f504d81073
Create Date: 2023-03-14 16:58:50.857993

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "d549ac6924c3"
down_revision = "13f504d81073"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "skills",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("name", sa.String(20)),
        sa.Column("level", sa.Integer),
        sa.Column("user_id", sa.Integer, sa.ForeignKey("users.id")),
        sa.Column("created_at", sa.DateTime(timezone=True), server_default=sa.sql.func.now()),
        sa.Column("updated_at", sa.DateTime(timezone=True), onupdate=sa.sql.func.now()),
    )

    op.add_column("users", sa.Column("skills", sa.Integer))
    op.create_foreign_key("user_skill_rel", "skills", "users", ["user_id"], ["id"])


def downgrade() -> None:
    op.drop_table("skills")
    op.drop_column("users", "skills")
