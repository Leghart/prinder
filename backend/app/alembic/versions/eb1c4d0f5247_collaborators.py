"""collaborators

Revision ID: eb1c4d0f5247
Revises: ce9396464c79
Create Date: 2023-04-02 14:05:43.939409

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "eb1c4d0f5247"
down_revision = "ce9396464c79"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "user_project",
        sa.Column("user_id", sa.Integer(), primary_key=True),
        sa.Column("project_id", sa.Integer(), primary_key=True),
    )
    op.add_column("users", sa.Column("contributions", sa.Integer))
    op.add_column("projects", sa.Column("collaborators", sa.Integer))

    op.create_foreign_key("relation_table_left_id_fkey_user_project", "user_project", "users", ["user_id"], ["id"])
    op.create_foreign_key(
        "relation_table_right_id_fkey_user_project", "user_project", "projects", ["project_id"], ["id"]
    )


def downgrade() -> None:
    op.drop_table("user_project")
    op.drop_column("users", "contributions")
    op.drop_column("projects", "collaborators")
