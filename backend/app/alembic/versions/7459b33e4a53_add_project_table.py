"""add project table

Revision ID: 7459b33e4a53
Revises: 316475c23885
Create Date: 2023-03-28 17:42:37.750786

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "7459b33e4a53"
down_revision = "316475c23885"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column("users", sa.Column("projects", sa.Integer))
    op.create_table(
        "projects",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("name", sa.String(30), unique=True),
        sa.Column("start_date", sa.DateTime(timezone=True)),
        sa.Column("end_date", sa.DateTime(timezone=True), nullable=True, default=None),
        sa.Column("description", sa.Text, nullable=True, default=None),
        sa.Column("url", sa.Text, nullable=True, default=None),
        sa.Column("archived", sa.Boolean, default=False),
        sa.Column("created_at", sa.DateTime(timezone=True), server_default=sa.sql.func.now()),
        sa.Column("updated_at", sa.DateTime(timezone=True), onupdate=sa.sql.func.now()),
        sa.Column("owner_id", sa.Integer, sa.ForeignKey("users.id")),
    )


def downgrade() -> None:
    op.drop_column("users", "projects")
    op.drop_table("projects")
