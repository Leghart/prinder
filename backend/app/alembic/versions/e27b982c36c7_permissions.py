"""permissions

Revision ID: e27b982c36c7
Revises: bcc31e5e377a
Create Date: 2023-05-20 16:25:59.280891

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "e27b982c36c7"
down_revision = "bcc31e5e377a"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "permissions",
        sa.Column("id", sa.Integer(), primary_key=True),
        sa.Column("table", sa.String(30)),
        sa.Column("name", sa.String(30)),
    )
    op.create_unique_constraint("uq_permission_table_name", "permissions", ["table", "name"])

    op.create_table(
        "user_permission",
        sa.Column("user_id", sa.Integer(), primary_key=True),
        sa.Column("permission_id", sa.Integer(), primary_key=True),
    )
    op.add_column("users", sa.Column("permissions", sa.Integer))
    op.add_column("permissions", sa.Column("users", sa.Integer))

    op.create_foreign_key(
        "relation_table_left_id_fkey_user_permission",
        "user_permission",
        "users",
        ["user_id"],
        ["id"],
    )
    op.create_foreign_key(
        "relation_table_right_id_fkey_user_permission", "user_permission", "permissions", ["permission_id"], ["id"]
    )


def downgrade() -> None:
    op.drop_table("user_permission")
    op.drop_table("permissions")
    op.drop_column("users", "permissions")
    op.drop_column("permissions", "users")
