"""superuser

Revision ID: d925f1b03710
Revises: e27b982c36c7
Create Date: 2023-06-01 18:53:43.061226

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "d925f1b03710"
down_revision = "e27b982c36c7"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column("users", sa.Column("is_superuser", sa.Boolean, default=False))


def downgrade() -> None:
    op.drop_column("users", "is_superuser")
