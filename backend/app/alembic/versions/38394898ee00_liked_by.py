"""liked_by

Revision ID: 38394898ee00
Revises: eb1c4d0f5247
Create Date: 2023-04-08 16:19:17.367840

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "38394898ee00"
down_revision = "eb1c4d0f5247"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "projects_liked_by_users",
        sa.Column("user_id", sa.Integer(), primary_key=True),
        sa.Column("project_id", sa.Integer(), primary_key=True),
    )
    op.add_column("users", sa.Column("liked_projects", sa.Integer))
    op.add_column("projects", sa.Column("liked_by", sa.Integer))
    op.create_foreign_key(
        "relation_table_left_id_fkey_liked_by",
        "projects_liked_by_users",
        "users",
        ["user_id"],
        ["id"],
    )
    op.create_foreign_key(
        "relation_table_right_id_fkey_liked_by",
        "projects_liked_by_users",
        "projects",
        ["project_id"],
        ["id"],
    )


def downgrade() -> None:
    op.drop_table("projects_liked_by_users")
    op.drop_column("users", "liked_projects")
    op.drop_column("projects", "liked_by")
