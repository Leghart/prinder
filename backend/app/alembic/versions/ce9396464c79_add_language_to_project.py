"""add language to project

Revision ID: ce9396464c79
Revises: 7459b33e4a53
Create Date: 2023-04-01 16:29:54.250957

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "ce9396464c79"
down_revision = "7459b33e4a53"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column("projects", sa.Column("language_id", sa.Integer))


def downgrade() -> None:
    op.drop_column("projects", "language_id")
