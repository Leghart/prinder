"""init

Revision ID: 5f4373f31272
Revises: 
Create Date: 2023-02-23 19:52:40.992393

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "5f4373f31272"
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "users",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("name", sa.String, nullable=False),
        sa.Column("age", sa.Integer, nullable=False),
    )


def downgrade() -> None:
    op.drop_table("users")
