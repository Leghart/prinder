"""add_technology_table

Revision ID: bcc31e5e377a
Revises: 38394898ee00
Create Date: 2023-04-09 15:15:54.282633

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "bcc31e5e377a"
down_revision = "38394898ee00"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "technologies",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("name", sa.String(30)),
        sa.Column("version", sa.String(10)),
        sa.Column("project_id", sa.Integer, sa.ForeignKey("projects.id")),
        sa.Column("created_at", sa.DateTime(timezone=True), server_default=sa.sql.func.now()),
        sa.Column("updated_at", sa.DateTime(timezone=True), onupdate=sa.sql.func.now()),
    )

    op.add_column("projects", sa.Column("technologies", sa.Integer))
    op.create_foreign_key("project_technology_rel", "technologies", "projects", ["project_id"], ["id"])


def downgrade() -> None:
    op.drop_table("technologies")
    op.drop_column("projects", "technologies")
