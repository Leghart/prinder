"""user table

Revision ID: 658074b38158
Revises: 5f4373f31272
Create Date: 2023-02-26 23:20:08.855859

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "658074b38158"
down_revision = "5f4373f31272"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.drop_table("users")
    op.create_table(
        "users",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("first_name", sa.String(20), nullable=False),
        sa.Column("last_name", sa.String(20), nullable=False),
        sa.Column("email", sa.String(30), unique=True, nullable=False),
        sa.Column("bio", sa.Text, nullable=True, default=None),
        sa.Column("image", sa.String, nullable=True, default=None),
        sa.Column("username", sa.String(20), nullable=False, unique=True),
        sa.Column("password", sa.String(50)),
        sa.Column("created_at", sa.DateTime(timezone=True), server_default=sa.sql.func.now()),
        sa.Column("updated_at", sa.DateTime(timezone=True), onupdate=sa.sql.func.now()),
    )


def downgrade() -> None:
    op.drop_table("users")
