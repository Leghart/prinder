"""language table

Revision ID: 13f504d81073
Revises: 658074b38158
Create Date: 2023-03-07 17:51:10.658425

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "13f504d81073"
down_revision = "658074b38158"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "user_language",
        sa.Column("user_id", sa.Integer(), primary_key=True),
        sa.Column("language_id", sa.Integer(), primary_key=True),
    )

    op.create_table(
        "languages",
        sa.Column("id", sa.Integer(), primary_key=True),
        sa.Column("name", sa.String(20)),
        sa.Column("level", sa.String(2)),
        sa.Column("users", sa.Integer()),
        sa.Column("created_at", sa.DateTime(timezone=True), server_default=sa.sql.func.now()),
        sa.Column("updated_at", sa.DateTime(timezone=True), onupdate=sa.sql.func.now()),
    )

    op.add_column("users", sa.Column("languages", sa.Integer))

    op.create_foreign_key("relation_table_left_id_fkey", "user_language", "users", ["user_id"], ["id"])
    op.create_foreign_key("relation_table_right_id_fkey", "user_language", "languages", ["language_id"], ["id"])
    op.create_unique_constraint("uq_language_name_level", "languages", ["name", "level"])


def downgrade() -> None:
    op.drop_table("users")
    op.drop_table("languages")
    op.drop_table("user_language")
