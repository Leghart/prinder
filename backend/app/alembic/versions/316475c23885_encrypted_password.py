"""encrypted_password

Revision ID: 316475c23885
Revises: d549ac6924c3
Create Date: 2023-03-18 12:56:43.013644

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "316475c23885"
down_revision = "d549ac6924c3"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.alter_column(
        "users",
        "password",
        existing_type=sa.String(50),
        type_=sa.LargeBinary(60),
        postgresql_using="password::bytea",
    )


def downgrade() -> None:
    op.alter_column(
        "users",
        "password",
        existing_type=sa.LargeBinary(60),
        type_=sa.String(50),
    )
