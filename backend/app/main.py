from fastapi import FastAPI, Request, Response
from fastapi.middleware.cors import CORSMiddleware
from fastapi_sqlalchemy import DBSessionMiddleware
from sqladmin import Admin

import app.admin as adm

from .database import SessionLocal, engine
from .router import router as api_router
from .settings import settings

desciption = """
API helps you do every necessary action on resources. Gives possibility to **read**, **list**, **create**, **update** and **destroy**
records from database. Each table has the same interface of CRUD (and optionaly additional endpoints for custom actions).
Be careful which content-type you send. In general *application/json* is enough but in some cases required is *multipart/form*.

## User
Despite CRUD, you will be able to:
* **add** image, skill or language to user,
* **remove** image, skill or language from user

## Project
Despite CRUD, you will be able to:
* **add** collaborator, technology or users who like project
* **remove** collaborator, technology or users who like project

## Admin
Despite CRUD, you will be able to:
* **add** permissions to user
* **remove** permissions from user
* **read** permissions of user
"""

tags_metadata = [
    {"name": "User", "description": "Operations with users. Provides typical **CRUD** actions and custom ednpoints."},
    {"name": "Skill", "description": "Operations with skills. Provides typical **CRUD**."},
    {"name": "Technology", "description": "Operations with technologies. Provides typical **CRUD** actions."},
    {"name": "Language", "description": "Operations with languages. Provides typical **CRUD** actions."},
    {
        "name": "Project",
        "description": "Operations with projects. Provides typical **CRUD** actions and custom ednpoints.",
    },
    {"name": "Authentication", "description": "Operations related to user authentication. Provides **login**."},
    {"name": "Admin", "description": "Operations with admin (superuser) actions."},
]

app = FastAPI(
    title="Prinder",
    description=desciption,
    version="0.1.0",
    license_info={
        "name": "Apache 2.0",
        "url": "https://www.apache.org/licenses/LICENSE-2.0.html",
    },
    openapi_tags=tags_metadata,
)

app.add_middleware(DBSessionMiddleware, db_url=settings.DATABASE_URL)
app.add_middleware(
    CORSMiddleware,
    allow_origins=settings.ORIGINS,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.middleware("http")
async def db_session_middleware(request: Request, call_next):
    response = Response("Internal server error", status_code=500)
    try:
        request.state.db = SessionLocal()
        response = await call_next(request)
    finally:
        request.state.db.close()
    return response


app.include_router(api_router)


admin = Admin(app, engine=engine)

admin.add_view(adm.UserAdmin)
admin.add_view(adm.LanguageAdmin)
admin.add_view(adm.PermissionAdmin)
admin.add_view(adm.ProjectAdmin)
admin.add_view(adm.TechnologyAdmin)
admin.add_view(adm.SkillAdmin)
