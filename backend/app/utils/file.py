import os
import uuid

import aiofiles
from fastapi import HTTPException, UploadFile, status

from app.settings import settings


async def handle_file_upload(file: UploadFile) -> str:
    if not isinstance(file.filename, str):
        raise HTTPException(status_code=status.HTTP_406_NOT_ACCEPTABLE, detail="Filename is incorrect.")

    _, ext = os.path.splitext(file.filename)

    content = await file.read()

    if file.content_type not in ("image/jpeg", "image/png", "image/jpg"):
        raise HTTPException(status_code=status.HTTP_406_NOT_ACCEPTABLE, detail="Only .jpeg or .png files allowed")

    file_name = f"{uuid.uuid4().hex}{ext}"
    file_path = os.path.join(settings.STATIC_FILES_PATH, file_name)
    async with aiofiles.open(file_path, "wb") as f:
        await f.write(content)

    return file_path


def delete_file(file_path: str) -> None:
    try:
        os.remove(file_path)
    except OSError:
        pass
