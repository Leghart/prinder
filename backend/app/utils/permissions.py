from typing import Optional

from fastapi import Depends, HTTPException, status
from sqlalchemy.orm import Session

from app import models
from app.actions.permission import PermissionActions
from app.utils.auth import get_current_user

from ..database import get_db


class PermissionsChecker:
    def __init__(self, required_permissions: list[str]):
        self._permissions = required_permissions

    def __call__(self, user: Optional[models.User] = Depends(get_current_user), db: Session = Depends(get_db)):
        """Checks if user has each of passed permission to endpoint.

        Can be used directly inside arguments for custom endpoint.
        """
        self.check(user=user, db=db)

    def check(self, user: Optional[models.User] = Depends(get_current_user), db: Session = Depends(get_db)):
        """Checks if user has each of passed permission to endpoint.

        If permission has not be checking, you can set
        `required_permission=None`.

        For `user=None`, validation will be skipped (WA for tests :/).
        """
        if not user:
            return

        for permission in self._permissions:
            if permission is None:
                continue
            table, name = permission.split(".")
            perm = PermissionActions.get(name=name, table=table, db=db)
            if not user.has_perm(permission=perm):
                raise HTTPException(
                    status_code=status.HTTP_401_UNAUTHORIZED,
                    detail="You dont have permission for this resource",
                )
