from datetime import datetime, timedelta
from typing import Annotated, Optional

from fastapi import Depends, HTTPException, Security, status
from fastapi.security import OAuth2PasswordBearer, SecurityScopes
from jose import JWTError, jwt
from passlib.context import CryptContext
from sqlalchemy.orm import Session

from app import schemas
from app.actions.user import UserActions
from app.database import get_db
from app.models.user import User
from app.settings import settings

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token", scopes={"read": "Read item", "list": "Read many items"})


def verify_password(plain_password: str, hashed_password: bytes) -> bool:
    """Validates password entered with the one saved in database."""
    return pwd_context.verify(plain_password, hashed_password)


def encrypt_password(password: str) -> bytes:
    """Hashes and returns the entered password."""
    return pwd_context.hash(password).encode()


def get_user(username: str, db: Session) -> Optional[schemas.user.ToToken]:
    """Gets user from db and transforms it to hashed version.

    Adds permissions to representation to give possibility to read
    perms from jwt.
    """
    if user := UserActions.get(username=username, db=db):
        data = user.to_pydantic().dict() | {"permissions": user.permissions}
        return schemas.user.ToToken(**data)
    return None


def authenticate_user(username: str, password: str, db: Session) -> Optional[schemas.user.ToToken]:
    """Returns user if verification passed."""
    user = get_user(username, db)
    if not user:
        return None

    if not verify_password(password, user.password):
        return None

    return user


def create_access_token(data: dict, expires_delta: timedelta | None = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta

    else:
        expire = datetime.utcnow() + timedelta(minutes=15)

    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, settings.SECRET_KEY, algorithm=settings.ALGORITHM)
    return encoded_jwt


async def get_current_user(
    security_scopes: SecurityScopes, token: Annotated[str, Depends(oauth2_scheme)], db: Session = Depends(get_db)
) -> User:
    if security_scopes.scopes:
        authenticate_value = f'Bearer scope="{security_scopes.scope_str}"'

    else:
        authenticate_value = "Bearer"

    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": authenticate_value},
    )

    try:
        payload = jwt.decode(token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM])
        username: str = payload.get("username")

        if username is None:
            raise credentials_exception

        token_scopes = payload.get("scopes", [])
        token_data = schemas.token.TokenExtraDataSchema(username=username, scopes=token_scopes)

    except JWTError:
        raise credentials_exception

    if token_data.username:
        user = UserActions.get(username=token_data.username, db=db)

    if user is None:
        raise credentials_exception

    for scope in security_scopes.scopes:
        if scope not in token_data.scopes:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Not enough permissions",
                headers={"WWW-Authenticate": authenticate_value},
            )

    return user


def authentication(scopes: list[str]) -> User | None:
    """Decides whether use authentication.

    Variables `AUTH` should always be "True", but for testing and
    development could be "False" -> no scope will be taken.
    """
    if settings.AUTH == "True":
        return Security(get_current_user, scopes=scopes)

    return Security(lambda: None, scopes=[])
