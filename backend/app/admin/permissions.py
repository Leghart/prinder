from sqladmin import ModelView

from app.models.permissions import Permission


class PermissionAdmin(ModelView, model=Permission):
    page_size = 50
    page_size_options = [10, 25, 50, 100]
    column_list = [Permission.id, Permission.table, Permission.name]
