from sqladmin import ModelView

from app.models.project import Project


class ProjectAdmin(ModelView, model=Project):
    page_size = 50
    page_size_options = [10, 25, 50, 100]
    column_list = [Project.id, Project.name, Project.start_date, Project.end_date]
