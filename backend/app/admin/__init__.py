from .language import LanguageAdmin
from .permissions import PermissionAdmin
from .project import ProjectAdmin
from .skill import SkillAdmin
from .technology import TechnologyAdmin
from .user import UserAdmin
