from sqladmin import ModelView

from app.models.language import Language


class LanguageAdmin(ModelView, model=Language):
    page_size = 50
    page_size_options = [10, 25, 50, 100]
    column_list = [Language.id, Language.name, Language.level]
