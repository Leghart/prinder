from sqladmin import ModelView

from app.models.user import User


class UserAdmin(ModelView, model=User):
    page_size = 50
    page_size_options = [10, 25, 50, 100]
    column_list = [User.id, User.first_name, User.last_name, User.username, User.is_superuser]

    form_excluded_columns = [User.password]
