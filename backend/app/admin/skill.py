from sqladmin import ModelView

from app.models.skill import Skill


class SkillAdmin(ModelView, model=Skill):
    page_size = 50
    page_size_options = [10, 25, 50, 100]
    column_list = [Skill.id, Skill.name, Skill.level]
