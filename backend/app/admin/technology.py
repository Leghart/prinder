from sqladmin import ModelView

from app.models.technology import Technology


class TechnologyAdmin(ModelView, model=Technology):
    page_size = 50
    page_size_options = [10, 25, 50, 100]
    column_list = [Technology.id, Technology.name, Technology.version]
