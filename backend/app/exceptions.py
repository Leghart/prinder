import re
from abc import ABC


class GeneralError(Exception, ABC):
    def __init__(self, message: str):
        self.message = message


class AlreadyExistsError(GeneralError):
    @property
    def detail(self):
        if mess := re.search("DETAIL:(.+)", self.message):
            return mess.group(1)
        return self.message


class RecordDoesntExist(GeneralError):
    ...


class ValidationError(GeneralError):
    ...
