from app.models import Language

from .. import conftest


def test_get_language(language_factory, client):
    language = language_factory.create()
    response = client.get(f"/languages/{language.id}")

    assert response.status_code == 200
    assert response.json() == {"id": language.id, "name": language.name, "level": language.level}


def test_get_many_languages(language_factory, client):
    language_factory.create_batch(4)

    response = client.get("/languages")

    assert isinstance(response.json(), list)
    assert len(response.json()) == 4


def test_create_language_unique_pair_fields_exists(language_factory, client):
    language_factory.create(name="eng", level="a1")

    payload = {"name": "eng", "level": "a1"}
    response = client.post("/languages", json=payload)

    assert response.json()["detail"] == "  Key (level, name)=(a1, eng) already exists."
    assert response.status_code == 409


def test_create_language_part_unique_pair_exist(language_factory, client):
    language_factory.create(name="eng", level="a1")

    payload = {"name": "deu", "level": "a1"}
    response = client.post("/languages", json=payload)

    assert response.status_code == 201


def test_create_language_incomplete_data(client):
    response = client.post("/languages", json={})

    missing_fields = ("name", "level")

    assert response.status_code == 422
    for err, miss in zip(response.json()["detail"], missing_fields):
        assert err["loc"][1] == miss
        assert err["msg"] == "field required"
        assert err["type"] == "value_error.missing"


def test_patch_language_partial_update(language_factory, db, client):
    language = language_factory.create(name="test_name", level="c1")

    response = client.patch(f"/languages/{language.id}", json={"name": "new_name"})

    language_db = db.query(Language).first()
    db.refresh(language_db)

    assert response.json() == {"id": language.id, "name": language.name, "level": language.level}

    assert language_db.id == language.id
    assert language_db.level == language.level
    assert language_db.name == "new_name"


def test_delete_language(db, client, language_factory):
    languages = language_factory.create_batch(2)

    client.delete(f"/languages/{languages[0].id}")

    assert db.query(Language).count() == 1
