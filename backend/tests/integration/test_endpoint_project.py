from datetime import datetime

from app.models import Language, Project, Technology, User

from .. import conftest


def test_get_project_with_related_fields(project_factory, user_factory, language, technology, client, db):
    users = user_factory.create_batch(4)
    project = project_factory.create(
        language=language,
        owner=users[0],
        add_collaborators=[users[1], users[2]],
        add_technologies=[technology],
        liked_by=[users[3]],
    )

    language = db.query(Language).first()

    response = client.get(f"/projects/{project.id}")

    assert response.status_code == 200
    assert response.json() == {
        "name": project.name,
        "url": project.url,
        "description": project.description,
        "id": project.id,
        "owner": {"id": users[0].id},
        "liked_by": [{"id": users[3].id}],
        "language": {"name": language.name, "level": language.level, "id": language.id},
        "collaborators": [{"id": users[1].id}, {"id": users[2].id}],
        "technologies": [{"id": technology.id, "name": technology.name, "version": technology.version}],
        "start_date": datetime.strftime(project.start_date, Project.DATE_FORMAT),
        "end_date": datetime.strftime(project.end_date, Project.DATE_FORMAT),
        "archived": project.archived,
    }


def test_get_many_projects(project_factory, client):
    project_factory.create_batch(4)

    response = client.get("/projects")

    assert isinstance(response.json(), list)
    assert len(response.json()) == 4


def test_create_project_already_exists(client, language, user, project):
    payload = {
        "name": project.name,
        "url": "abc",
        "start_date": "20-11-2012",
        "end_date": "21-12-2022",
        "language_id": language.id,
        "owner_id": user.id,
    }
    response = client.post("/projects", json=payload)

    assert response.status_code == 409
    assert response.json()["detail"] == f"  Key (name)=({project.name}) already exists."


def test_create_project_incomplete_data(client):
    response = client.post("/projects", json={})

    missing_fields = ("name", "start_date", "end_date", "owner_id", "language_id")

    assert response.status_code == 422
    for err, miss in zip(response.json()["detail"], missing_fields):
        assert err["loc"][1] == miss
        assert err["msg"] == "field required"
        assert err["type"] == "value_error.missing"


def test_patch_project_partial_update(project_factory, db, client):
    project = project_factory.create(name="test_projectname", url="test url")

    client.patch(f"/projects/{project.id}", json={"name": "new name"})

    project_db = db.query(Project).first()
    db.refresh(project_db)

    assert project_db.id == project.id
    assert project_db.name == "new name"


def test_delete_project(project_factory, db, client):
    projects = project_factory.create_batch(2)

    client.delete(f"/projects/{projects[0].id}")

    assert db.query(Project).count() == 1


def test_add_collaborator_which_doesnt_exist(project_factory, client, db):
    project = project_factory.create()

    response = client.get(f"/projects/{project.id}")

    assert response.json()["collaborators"] == []

    response = client.post(f"/projects/{project.id}/collaborators", json={"id": 1012})

    assert response.json()["detail"] == "Object does not exist (User, id=1012)."


def test_add_contributor_to_project(project_factory, user_factory, client, db):
    project = project_factory.create()
    user = user_factory.create()

    response = client.post(f"/projects/{project.id}/collaborators", json={"id": user.id})

    assert response.json() == [{"id": user.id}]

    assert db.query(Project).first().collaborators == [user]


def test_remove_contributor_from_project(project_factory, user_factory, client, db):
    users = user_factory.create_batch(3)
    project = project_factory.create(add_collaborators=[users[0], users[1]], owner=users[2])

    assert db.query(User).count() == 3

    client.delete(f"/projects/{project.id}/collaborators/{users[0].id}")

    assert project.collaborators == [users[1]]
    assert db.query(User).count() == 3


def test_add_users_like_to_project(project_factory, user_factory, client, db):
    project = project_factory.create()
    user = user_factory.create()

    response = client.post(f"/projects/{project.id}/likes", json={"id": user.id})

    assert response.json() == [{"id": user.id}]

    assert db.query(Project).first().liked_by == [user]


def test_remove_users_likes_from_project(project_factory, user_factory, client, db):
    users = user_factory.create_batch(3)
    project = project_factory.create(liked_by=[users[0], users[1]], owner=users[2])

    assert db.query(User).count() == 3

    client.delete(f"/projects/{project.id}/likes/{users[0].id}")

    assert project.liked_by == [users[1]]
    assert db.query(User).count() == 3


def test_add_technology_to_project(project, client, db):
    payload = {"name": "flask", "version": "0.1"}

    response = client.post(f"/projects/{project.id}/technologies", json=payload)

    technology = db.query(Technology).first()

    assert technology
    assert response.json() == [{"id": technology.id, "name": technology.name, "version": technology.version}]
    assert db.query(Project).first().technologies == [technology]


def test_remove_technology_from_project(project_factory, technology_factory, client, db):
    technologies = technology_factory.create_batch(3)
    project = project_factory.create(add_technologies=[technologies[0], technologies[1]])

    assert db.query(Technology).count() == 3

    client.delete(f"/projects/{project.id}/technologies/{technologies[0].id}")

    assert project.technologies == [technologies[1]]
    assert db.query(Technology).count() == 2
