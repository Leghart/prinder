from app.models import Skill

from .. import conftest


def test_get_skill(skill_factory, user, client):
    skill = skill_factory.create(user_id=user.id)

    response = client.get(f"/skills/{skill.id}")

    assert response.status_code == 200
    assert response.json() == {"id": skill.id, "name": skill.name, "level": skill.level, "user_id": skill.user_id}


def test_get_many_skills(skill_factory, user, client):
    skill_factory.create_batch(4, user_id=user.id)

    response = client.get("/skills")

    assert isinstance(response.json(), list)
    assert len(response.json()) == 4


def test_patch_skill_partial_update(skill_factory, user, db, client):
    skill = skill_factory.create(name="test_name", level=1, user=user)

    response = client.patch(f"/skills/{skill.id}", json={"name": "new_name"})

    skill_db = db.query(Skill).first()
    db.refresh(skill_db)

    assert response.json() == {"id": skill.id, "name": skill.name, "level": skill.level, "user_id": skill.user_id}

    assert skill_db.id == skill.id
    assert skill_db.level == skill.level
    assert skill_db.name == "new_name"
    assert skill_db.user_id == user.id


def test_delete_skill(db, client, user, skill_factory):
    skills = skill_factory.create_batch(2, user_id=user.id)

    client.delete(f"/skills/{skills[0].id}")

    assert db.query(Skill).count() == 1


def test_add_skill(db, client, user):
    payload = {"user_id": user.id, "name": "python", "level": 2}
    response = client.post("/skills", json=payload)

    assert response.json() == {
        "id": 1,
        "name": "python",
        "level": 2,
        "user_id": user.id,
    }

    assert db.query(Skill).count() == 1
