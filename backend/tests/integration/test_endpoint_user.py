from unittest.mock import AsyncMock, patch

import pytest

from app.actions import UserActions
from app.models import Language, Skill, User

from .. import conftest


def test_get_user_with_related_fields(user_factory, language, skill, client, db):
    user = user_factory.create(add_languages=[language], add_skills=[skill])

    language = db.query(Language).first()
    skill = db.query(Skill).first()

    response = client.get(f"/users/{user.id}")

    assert response.status_code == 200
    assert response.json() == {
        "id": user.id,
        "first_name": user.first_name,
        "last_name": user.last_name,
        "username": user.username,
        "email": user.email,
        "owned": [],
        "liked_projects": [],
        "contributions": [],
        "languages": [
            {
                "id": language.id,
                "name": language.name,
                "level": language.level,
            }
        ],
        "skills": [
            {
                "id": skill.id,
                "name": skill.name,
                "level": skill.level,
            }
        ],
        "bio": user.bio,
        "image": None,
    }


def test_get_many_users(user_factory, client):
    user_factory.create_batch(4)

    response = client.get("/users")

    assert isinstance(response.json(), list)
    assert len(response.json()) == 4


@pytest.mark.parametrize("field", ("username", "email"))
def test_create_user_unique_fields_exists(field, user_factory, client, db):
    kwargs = {field: "abc"}
    user_factory.create(**kwargs)

    base_payload = {"username": "test", "email": "test"} | kwargs

    payload = {"first_name": "awada", "last_name": "kedavra", "password": "nadnbad"} | base_payload
    response = client.post("/users", json=payload)

    assert response.json()["detail"] == f"  Key ({field})=(abc) already exists."
    assert response.status_code == 409


def test_create_user_incomplete_data(client):
    response = client.post("/users", json={})

    missing_fields = ("first_name", "last_name", "email", "username", "password")

    assert response.status_code == 422
    for err, miss in zip(response.json()["detail"], missing_fields):
        assert err["loc"][1] == miss
        assert err["msg"] == "field required"
        assert err["type"] == "value_error.missing"


def test_patch_user_partial_update(user_factory, db, client):
    user = user_factory.create(username="test_username", email="test_email")

    response = client.patch(f"/users/{user.id}", json={"username": "new_username"})

    user_db = db.query(User).first()
    db.refresh(user_db)

    assert response.json() == {
        "id": user.id,
        "bio": user.bio,
        "email": user.email,
        "first_name": user.first_name,
        "image": user.image,
        "languages": [],
        "skills": [],
        "liked_projects": [],
        "owned": [],
        "contributions": [],
        "last_name": user.last_name,
        "username": user.username,
    }

    assert user_db.id == user.id
    assert user_db.first_name == user.first_name
    assert user_db.last_name == user.last_name
    assert user_db.email == user.email
    assert user_db.username == "new_username"


def test_delete_user(db, client, user_factory):
    users = user_factory.create_batch(2)

    client.delete(f"/users/{users[0].id}")

    assert db.query(User).count() == 1
    assert db.query(User).first().id == users[1].id


def test_add_languages_which_doesnt_exist(user_factory, client, db):
    user = user_factory.create()

    response = client.get(f"/users/{user.id}")

    assert response.json()["languages"] == []

    payload = {"id": 69}
    response = client.post(f"/users/{user.id}/languages", json=payload)
    assert response.json()["detail"] == "Object does not exist (Language, id=69)."


def test_add_languages_to_user(user_factory, language, client, db):
    user = user_factory.create()

    response = client.post(f"/users/{user.id}/languages", json={"id": language.id})

    assert response.json() == [{"id": language.id, "name": language.name, "level": language.level}]

    assert db.query(User).first().languages == [language]


def test_remove_languages_from_user(user_factory, language_factory, client, db):
    user = user_factory.create(add_languages=language_factory.create_batch(2))

    assert db.query(Language).count() == 2

    languages = db.query(Language).all()

    response = client.delete(f"/users/{user.id}/languages/{languages[0].id}")

    assert response.json() == [{"name": languages[1].name, "level": languages[1].level, "id": languages[1].id}]
    assert user.languages == [languages[1]]
    assert db.query(Language).count() == 2


def test_add_skill_to_user(user_factory, client, db):
    user = user_factory.create()
    payload = {"name": "python", "level": 5}
    response = client.post(f"/users/{user.id}/skills", json=payload)

    assert response.json() == [{"id": 1, "name": "python", "level": 5, "user_id": user.id}]

    assert db.query(User).first().skills == db.query(Skill).all()


def test_remove_skill_from_user(user_factory, skill_factory, client, db):
    user = user_factory.create()
    skill = skill_factory.create(user_id=user.id)

    assert db.query(Skill).count() == 1

    response = client.delete(f"/users/{user.id}/skills/{skill.id}")

    assert response.json() == []
    assert user.skills == []
    assert db.query(Skill).count() == 0


@patch("app.actions.user.delete_file")
@patch("app.actions.user.handle_file_upload")
async def test_upload_image_change_image_and_delete_previous_image(handle_mock, delete_mock, user_factory, db):
    handle_mock.return_value = "image_path"
    user = user_factory.create(image="old_path")

    result = await UserActions.add_image(user.id, image=AsyncMock(), db=db)

    delete_mock.assert_called_once_with("old_path")
    assert result.image == "image_path"


@patch("app.actions.user.delete_file")
def test_remove_image(delete_mock, user_factory, db):
    user = user_factory.create(image="test")
    result = UserActions.remove_image(user.id, db=db)

    delete_mock.assert_called_once_with("test")
    assert result.image is None


@patch("app.actions.user.delete_file")
def test_remove_image_not_delete(delete_mock, user, db):
    result = UserActions.remove_image(user.id, db=db)

    delete_mock.assert_not_called()
    assert result.image is None
