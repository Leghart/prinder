from app.models import Technology

from .. import conftest


def test_get_technology(technology_factory, project, client):
    technology = technology_factory.create(project_id=project.id)

    response = client.get(f"/technologies/{technology.id}")

    assert response.status_code == 200
    assert response.json() == {
        "id": technology.id,
        "name": technology.name,
        "version": technology.version,
        "project_id": technology.project_id,
    }


def test_get_many_technologies(technology_factory, project, client):
    technology_factory.create_batch(4, project_id=project.id)

    response = client.get("/technologies")

    assert isinstance(response.json(), list)
    assert len(response.json()) == 4


def test_patch_technology_partial_update(technology_factory, project, db, client):
    technology = technology_factory.create(name="test_name", version=1, project=project)

    response = client.patch(f"/technologies/{technology.id}", json={"name": "new_name"})

    technology_db = db.query(Technology).first()
    db.refresh(technology_db)

    assert response.json() == {
        "id": technology.id,
        "name": technology.name,
        "version": technology.version,
        "project_id": technology.project_id,
    }

    assert technology_db.id == technology.id
    assert technology_db.version == technology.version
    assert technology_db.name == "new_name"
    assert technology_db.project_id == project.id


def test_delete_technology(db, client, project, technology_factory):
    technologies = technology_factory.create_batch(2, project_id=project.id)

    client.delete(f"/technologies/{technologies[0].id}")

    assert db.query(Technology).count() == 1


def test_add_technology(db, client, project):
    payload = {"project_id": project.id, "name": "django", "version": "1.2.3"}
    response = client.post("/technologies", json=payload)

    assert response.json() == {
        "id": 1,
        "name": "django",
        "version": "1.2.3",
        "project_id": project.id,
    }

    assert db.query(Technology).count() == 1
