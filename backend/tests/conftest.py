# mypy: disable-error-code=call-overload

from datetime import timedelta

import pytest
from factory.alchemy import SQLAlchemyModelFactory
from fastapi.testclient import TestClient
from pytest_factoryboy import register

from app.database import Base, TestingSessionLocal, engine_test, get_db, get_main_tables
from app.factories import (
    LanguageFactory,
    PermissionFactory,
    ProjectFactory,
    SkillFactory,
    TechnologyFactory,
    UserFactory,
)
from app.main import app
from app.utils.auth import create_access_token, encrypt_password, get_current_user


class FactoryTestManager:
    def __new__(cls, klass: SQLAlchemyModelFactory) -> SQLAlchemyModelFactory:
        klass._meta.sqlalchemy_session = TestingSessionLocal
        klass._meta.model.session = TestingSessionLocal
        return klass


register(FactoryTestManager(UserFactory), "user")
register(FactoryTestManager(LanguageFactory), "language")
register(FactoryTestManager(SkillFactory), "skill")
register(FactoryTestManager(ProjectFactory), "project")
register(FactoryTestManager(TechnologyFactory), "technology")


def override_get_db():
    try:
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()


def override_current_user():
    return None


app.dependency_overrides[get_db] = override_get_db
app.dependency_overrides[get_current_user] = override_current_user


@pytest.fixture(scope="session")
def client():
    yield TestClient(app)


@pytest.fixture()
def unauth_client():
    app.dependency_overrides[get_current_user] = get_current_user
    yield TestClient(app, headers={"Authorization": "Bearer xxx"})
    app.dependency_overrides[get_current_user] = override_current_user


@pytest.fixture(scope="function")
def auth_client():
    Base.metadata.create_all(bind=engine_test)
    app.dependency_overrides[get_current_user] = get_current_user
    basic_permissions = ("create", "read", "update", "delete", "list")
    basic_tables = get_main_tables()
    basic_tables.remove("permissions")

    all_permissions = []

    for table in basic_tables:
        for perm in basic_permissions:
            factory: SQLAlchemyModelFactory = FactoryTestManager(PermissionFactory)
            all_permissions.append(factory(table=table, name=perm))

    user = UserFactory(username="admin", password=encrypt_password("admin"), add_permissions=all_permissions)

    access_token_expires = timedelta(minutes=30)

    access_token = create_access_token(
        data={"username": user.username, "scopes": [f"{p.table}:{p.name}" for p in user.permissions]},
        expires_delta=access_token_expires,
    )

    yield TestClient(app, headers={"Authorization": f"Bearer {access_token}"})

    app.dependency_overrides[get_current_user] = override_current_user
    TestingSessionLocal.remove()
    Base.metadata.drop_all(bind=engine_test)


@pytest.fixture(autouse=True)
def db():
    Base.metadata.create_all(bind=engine_test)
    yield TestingSessionLocal
    TestingSessionLocal.remove()
    Base.metadata.drop_all(bind=engine_test)
