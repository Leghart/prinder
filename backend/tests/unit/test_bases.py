from unittest.mock import Mock, call, patch

import pytest
from fastapi import HTTPException
from pydantic import BaseModel
from sqlalchemy.exc import IntegrityError

from app.actions.base import ActionsBase, CrudBase
from app.exceptions import AlreadyExistsError


@pytest.fixture()
def crud_mock_class():
    model_mock = Mock()
    model_mock.__name__ = "classNameMock"

    class MockClass(CrudBase):
        _model = model_mock

    return MockClass


@pytest.fixture()
def actions_mock_class():
    model_mock = Mock()

    class MockClass(ActionsBase):
        _model = model_mock

    return MockClass


@pytest.fixture()
def db():
    return Mock()


class TestCrudBase:
    def test_crud_base_not_found_message(self, crud_mock_class):
        assert crud_mock_class._not_found_msg == "Object does not exist ({}, id={})."

    def test_create_method_unpack_pydantic_object(self, crud_mock_class, db):
        class PydanticMock(BaseModel):
            var: str
            number: int

        data = PydanticMock(var="test", number=1)

        crud_mock_class.create(data=data, db=db)

        crud_mock_class._model.assert_called_once_with(var="test", number=1)
        db.add.assert_called_once_with(crud_mock_class._model.return_value)
        db.commit.assert_called_once()

    def test_create_method_unpack_dictionary(self, crud_mock_class, db):
        data = {"var": "test", "number": 1}

        crud_mock_class.create(data=data, db=db)

        crud_mock_class._model.assert_called_once_with(var="test", number=1)
        db.add.assert_called_once_with(crud_mock_class._model.return_value)
        db.commit.assert_called_once()

    def test_create_method_raise_integrity_error(self, crud_mock_class, db):
        db.add.side_effect = IntegrityError(statement=Mock(), params=Mock(), orig=None)

        with pytest.raises(IntegrityError):
            crud_mock_class.create(data={}, db=db)

        db.rollback.assert_called_once()

    def test_create_method_raise_already_exists_error(self, crud_mock_class, db):
        orig = Mock()
        orig.args = ["DETAIL:test"]
        db.add.side_effect = IntegrityError(statement=Mock(), params=Mock(), orig=orig)

        with pytest.raises(AlreadyExistsError) as exc:
            crud_mock_class.create(data={}, db=db)

        db.rollback.assert_called_once()

        if exc._excinfo:
            assert exc._excinfo[1].detail == "test"

    def test_read_method_object_exists(self, crud_mock_class, db):
        obj_mock = Mock()
        db.get.return_value = obj_mock

        obj = crud_mock_class.read(id=1, db=db)

        db.get.assert_called_once_with(crud_mock_class._model, 1)
        assert obj is obj_mock

    def test_read_method_object_doesnt_exist(self, crud_mock_class, db):
        db.get.return_value = None

        with pytest.raises(HTTPException) as exc:
            crud_mock_class.read(id=1, db=db)

        db.get.assert_called_once_with(crud_mock_class._model, 1)
        if exc._excinfo:
            assert exc._excinfo[1].status_code == 404
            assert exc._excinfo[1].detail == "Object does not exist (classNameMock, id=1)."

    def test_list_method(self, crud_mock_class, db):
        db.query.return_value.all.return_value = ("test1", "test2")

        objs = crud_mock_class.list(db=db)

        db.query.assert_called_once_with(crud_mock_class._model)
        db.query.return_value.all.assert_called_once()
        assert objs == ("test1", "test2")

    def test_update_method_object_doesnt_exist(self, crud_mock_class, db):
        db.get.return_value = None

        with pytest.raises(HTTPException) as exc:
            crud_mock_class.update(id=1, data={}, db=db)

        db.get.assert_called_once_with(crud_mock_class._model, 1)
        if exc._excinfo:
            assert exc._excinfo[1].status_code == 404
            assert exc._excinfo[1].detail == "Object does not exist (classNameMock, id=1)."

    @patch("app.actions.base.setattr")
    def test_update_method(self, setattr_mock, crud_mock_class, db):
        obj_mock = Mock()
        db.get.return_value = obj_mock

        result = crud_mock_class.update(id=1, data={"attr1": "val1", "attr2": 2}, db=db)

        db.get.assert_called_once_with(crud_mock_class._model, 1)

        setattr_mock.assert_has_calls(
            [
                call(obj_mock, "attr1", "val1"),
                call(obj_mock, "attr2", 2),
            ]
        )
        db.commit.assert_called_once()
        db.refresh.assert_called_once_with(obj_mock)
        assert result == obj_mock

    def test_delete_method_object_doesnt_exist(self, crud_mock_class, db):
        db.get.return_value = None

        with pytest.raises(HTTPException) as exc:
            crud_mock_class.delete(id=1, db=db)

        db.get.assert_called_once_with(crud_mock_class._model, 1)
        if exc._excinfo:
            assert exc._excinfo[1].status_code == 404
            assert exc._excinfo[1].detail == "Object does not exist (classNameMock, id=1)."

    def test_delete_method(self, crud_mock_class, db):
        obj_mock = Mock()
        db.get.return_value = obj_mock

        result = crud_mock_class.delete(id=1, db=db)

        db.get.assert_called_once_with(crud_mock_class._model, 1)
        db.delete.assert_called_once_with(obj_mock)
        db.commit.assert_called_once()
        assert result == obj_mock


class TestActionsBase:
    def test_get_method_return_object(self, actions_mock_class, db):
        obj = actions_mock_class.get(id=1, field="abc", db=db)

        db.query.assert_called_once_with(actions_mock_class._model)
        db.query.return_value.filter_by.assert_called_once_with(id=1, field="abc")
        db.query.return_value.filter_by.return_value.first.assert_called_once()
        assert obj == db.query.return_value.filter_by.return_value.first.return_value

    @patch("app.actions.base.ActionsBase.get")
    def test_get_or_create_return_object(self, get_mock, actions_mock_class, db):
        get_mock.return_value = Mock()

        obj = actions_mock_class.get_or_create(id=1, field="abc", db=db)

        get_mock.assert_called_once_with(id=1, field="abc", db=db)
        assert obj == get_mock.return_value

    @patch("app.actions.base.ActionsBase.create")
    @patch("app.actions.base.ActionsBase.get")
    def test_get_or_create_create_object(self, get_mock, create_mock, actions_mock_class, db):
        get_mock.return_value = None

        obj = actions_mock_class.get_or_create(id=1, field="abc", db=db)

        get_mock.assert_called_once_with(id=1, field="abc", db=db)
        create_mock.assert_called_once_with({"id": 1, "field": "abc"}, db)
        assert obj == create_mock.return_value
