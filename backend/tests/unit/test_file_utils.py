from unittest.mock import AsyncMock, patch

import pytest
from fastapi import HTTPException

from app.settings import settings
from app.utils import file


@patch("app.utils.file.os")
def test_delete_file(os_mock):
    file.delete_file("test")

    os_mock.remove.assert_called_once_with("test")


@patch("app.utils.file.os")
def test_delete_file_caught_error(os_mock):
    os_mock.remove.side_effect = OSError
    file.delete_file("test")


async def test_file_upload_invalid_filename():
    image = AsyncMock(content_type="txt")

    with pytest.raises(HTTPException) as exc:
        await file.handle_file_upload(image)

    if exc._excinfo:
        assert exc._excinfo[1].detail == "Filename is incorrect."
        assert exc._excinfo[1].status_code == 406


async def test_file_upload_invalid_format():
    image = AsyncMock(content_type="txt")
    image.filename = "test"

    with pytest.raises(HTTPException) as exc:
        await file.handle_file_upload(image)

    if exc._excinfo:
        assert exc._excinfo[1].detail == "Only .jpeg or .png files allowed"
        assert exc._excinfo[1].status_code == 406


@patch("app.utils.file.aiofiles.open")
@patch("app.utils.file.os")
@patch("app.utils.file.uuid")
async def test_file_upload(uuid_mock, os_mock, open_mock):
    image = AsyncMock()
    image.filename = "test.jpg"
    image.content_type = "image/jpg"

    file_name = uuid_mock.uuid4.return_value.hex
    os_mock.path.splitext.return_value = ("", ".jpg")

    path = await file.handle_file_upload(image)

    os_mock.path.join.assert_called_once_with(settings.STATIC_FILES_PATH, f"{file_name}.jpg")
    open_mock.assert_called_once_with(os_mock.path.join.return_value, "wb")

    assert path == os_mock.path.join.return_value
