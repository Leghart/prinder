from app.settings import settings


def test_settings_have_required_fields():
    assert hasattr(settings, "DATABASE_URL_TEST")
    assert hasattr(settings, "DATABASE_URL")
