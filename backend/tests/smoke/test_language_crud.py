from .. import conftest


def test_get_languages(client):
    response = client.get("/languages")

    assert response.status_code == 200


def test_get_single_language(language_factory, client):
    language = language_factory.create()
    response = client.get(f"/languages/{language.id}")

    assert response.status_code == 200


def test_get_single_language_not_exists(client):
    response = client.get("/languages/69")

    assert response.status_code == 404


def test_create_language(client):
    payload = {"name": "english", "level": "a1"}
    response = client.post("/languages", json=payload)

    assert response.status_code == 201


def test_update_language(language_factory, client):
    language = language_factory.create()

    response = client.patch(f"/languages/{language.id}", json={"name": "polish"})

    assert response.status_code == 200


def test_update_language_not_exist(client):
    response = client.patch("/languages/69", json={"name": "polish"})

    assert response.status_code == 404
    assert response.json()["detail"] == "Object does not exist (Language, id=69)."


def test_delete_language(language_factory, client):
    language = language_factory.create()

    response = client.delete(f"/languages/{language.id}")

    assert response.status_code == 204


def test_delete_language_not_exist(client):
    response = client.delete("/languages/69")

    assert response.status_code == 404
    assert response.json()["detail"] == "Object does not exist (Language, id=69)."
