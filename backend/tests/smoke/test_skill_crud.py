from .. import conftest


def test_get_skills(client):
    response = client.get("/skills")

    assert response.status_code == 200


def test_get_single_skill(skill_factory, user, client):
    skill = skill_factory.create(user_id=user.id)
    response = client.get(f"/skills/{skill.id}")

    assert response.status_code == 200


def test_get_single_skill_not_exists(client):
    response = client.get("/skills/69")

    assert response.status_code == 404


def test_update_skill(skill_factory, user, client):
    skill = skill_factory.create(user_id=user.id)

    response = client.patch(f"/skills/{skill.id}", json={"name": "vue"})

    assert response.status_code == 200


def test_update_skill_not_exist(client):
    response = client.patch("/skills/69", json={"name": "python"})

    assert response.status_code == 404
    assert response.json()["detail"] == "Object does not exist (Skill, id=69)."


def test_delete_skill(skill_factory, user, client):
    skill = skill_factory.create(user_id=user.id)

    response = client.delete(f"/skills/{skill.id}")

    assert response.status_code == 204


def test_delete_skill_not_exist(client):
    response = client.delete("/skills/69")

    assert response.status_code == 404
    assert response.json()["detail"] == "Object does not exist (Skill, id=69)."
