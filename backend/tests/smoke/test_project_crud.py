import pytest

from .. import conftest


def test_get_projects(client):
    response = client.get("/projects")

    assert response.status_code == 200


def test_get_single_project(project_factory, client):
    project = project_factory.create()
    response = client.get(f"/projects/{project.id}")

    assert response.status_code == 200


def test_get_single_project_not_exists(client):
    response = client.get("/projects/69")

    assert response.status_code == 404


def test_create_project(client, language, user):
    payload = {
        "name": "a",
        "url": "abc",
        "start_date": "20-11-2012",
        "end_date": "21-12-2022",
        "language_id": language.id,
        "owner_id": user.id,
    }
    response = client.post("/projects", json=payload)
    assert response.status_code == 201


@pytest.mark.parametrize("date1,date2", (("20/02/2021", "20-02-2021"), ("20-02-2021", "20/02/2021")))
def test_create_project_invalid_date_format(date1, date2, client, language, user):
    payload = {
        "name": "abc",
        "url": "abc",
        "start_date": date1,
        "end_date": date2,
        "language_id": language.id,
        "owner_id": user.id,
    }
    response = client.post("/projects", json=payload)

    assert response.status_code == 400
    assert response.json()["detail"] == "time data '20/02/2021' does not match format '%d-%m-%Y'"


def test_update_project(project_factory, client):
    project = project_factory.create()

    response = client.patch(f"/projects/{project.id}", json={"url": "abc"})

    assert response.status_code == 200


def test_update_project_not_exist(client):
    response = client.patch("/projects/69", json={"description": "abc"})

    assert response.status_code == 404
    assert response.json()["detail"] == "Object does not exist (Project, id=69)."


def test_delete_project(project_factory, client):
    project = project_factory.create()

    response = client.delete(f"/projects/{project.id}")

    assert response.status_code == 204


def test_delete_project_not_exist(client):
    response = client.delete("/projects/69")

    assert response.status_code == 404
    assert response.json()["detail"] == "Object does not exist (Project, id=69)."


def test_add_collaborator(client, user, project):
    response = client.post(f"/projects/{project.id}/collaborators", json={"id": user.id})

    assert response.status_code == 200


def test_add_collaborator_project_doesnt_exist(client, user):
    response = client.post("/projects/69/collaborators", json={"id": user.id})

    assert response.status_code == 404
    assert response.json()["detail"] == "Object does not exist (Project, id=69)."


def test_remove_collaborator(client, user, project_factory):
    project = project_factory.create(add_collaborators=[user])
    response = client.delete(f"/projects/{project.id}/collaborators/{user.id}")

    assert response.status_code == 200


def test_remove_collaborator_project_doesnt_exist(client, user):
    response = client.delete(f"/projects/73/collaborators/{user.id}")

    assert response.status_code == 404
    assert response.json()["detail"] == "Object does not exist (Project, id=73)."


def test_add_like_user_to_project(client, user, project):
    response = client.post(f"/projects/{project.id}/likes", json={"id": user.id})

    assert response.status_code == 200


def test_add_like_user_project_doesnt_exist(client, user):
    response = client.post("/projects/69/likes", json={"id": user.id})

    assert response.status_code == 404
    assert response.json()["detail"] == "Object does not exist (Project, id=69)."


def test_remove_user_like(client, user, project_factory):
    project = project_factory.create(liked_by=[user])
    response = client.delete(f"/projects/{project.id}/likes/{user.id}")

    assert response.status_code == 200


def test_remove_user_like_project_doesnt_exist(client, user):
    response = client.delete(f"/projects/73/likes/{user.id}")

    assert response.status_code == 404
    assert response.json()["detail"] == "Object does not exist (Project, id=73)."
