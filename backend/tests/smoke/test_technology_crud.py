from .. import conftest


def test_get_technologies(client):
    response = client.get("/technologies")

    assert response.status_code == 200


def test_get_single_technology(technology_factory, project, client):
    technology = technology_factory.create(project_id=project.id)
    response = client.get(f"/technologies/{technology.id}")

    assert response.status_code == 200


def test_get_single_technology_not_exists(client):
    response = client.get("/technologies/69")

    assert response.status_code == 404


def test_update_technology(technology_factory, project, client):
    technology = technology_factory.create(project_id=project.id)

    response = client.patch(f"/technologies/{technology.id}", json={"name": "vue"})

    assert response.status_code == 200


def test_update_technology_not_exist(client):
    response = client.patch("/technologies/69", json={"name": "python"})

    assert response.status_code == 404
    assert response.json()["detail"] == "Object does not exist (Technology, id=69)."


def test_delete_technology(technology_factory, project, client):
    technology = technology_factory.create(project_id=project.id)

    response = client.delete(f"/technologies/{technology.id}")

    assert response.status_code == 204


def test_delete_technology_not_exist(client):
    response = client.delete("/technologies/69")

    assert response.status_code == 404
    assert response.json()["detail"] == "Object does not exist (Technology, id=69)."
