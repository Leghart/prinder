import pytest

from .. import conftest


@pytest.mark.parametrize("endpoint", ("users", "technologies", "skills", "projects", "languages"))
def test_read_permission_unauthenticated(unauth_client, endpoint):
    response = unauth_client.get(f"/{endpoint}/666/")

    assert response.status_code == 401
    assert response.headers["www-authenticate"] == f'Bearer scope="{endpoint}:read"'


@pytest.mark.parametrize(
    "endpoint,factory",
    (
        ("users", "user"),
        ("technologies", "technology"),
        ("skills", "skill"),
        ("languages", "language"),
        ("projects", "project"),
    ),
)
def test_read_permission_authenticated(auth_client, endpoint, factory, user, technology, skill, language, project):
    obj = locals()[factory]
    response = auth_client.get(f"/{endpoint}/{obj.id}/")

    assert response.status_code == 200


@pytest.mark.parametrize("endpoint", ("users", "technologies", "skills", "projects", "languages"))
def test_list_permission_unauthenticated(unauth_client, endpoint):
    response = unauth_client.get(f"/{endpoint}/")

    assert response.status_code == 401
    assert response.headers["www-authenticate"] == f'Bearer scope="{endpoint}:list"'


@pytest.mark.parametrize(
    "endpoint",
    ("users", "technologies", "skills", "languages", "projects"),
)
def test_list_permission_authenticated(auth_client, endpoint):
    response = auth_client.get(f"/{endpoint}/")

    assert response.status_code == 200
