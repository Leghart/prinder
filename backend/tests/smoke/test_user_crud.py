import os
from contextlib import contextmanager
from unittest.mock import patch

from .. import conftest


def test_get_users(client):
    response = client.get("/users")

    assert response.status_code == 200


def test_get_single_user(user_factory, client):
    user = user_factory.create()
    response = client.get(f"/users/{user.id}")

    assert response.status_code == 200


def test_get_single_user_not_exists(client):
    response = client.get("/users/69")

    assert response.status_code == 404


def test_create_user(client):
    payload = {
        "first_name": "a",
        "last_name": "a",
        "email": "abc@example.com",
        "username": "cba",
        "bio": "",
        "password": "nadnbad",
        "image": None,
    }
    response = client.post("/users", json=payload)

    assert response.status_code == 201


def test_update_user(user_factory, client):
    user = user_factory.create()

    response = client.patch(f"/users/{user.id}", json={"bio": "abc"})

    assert response.status_code == 200


def test_update_user_not_exist(client):
    response = client.patch("/users/69", json={"bio": "abc"})

    assert response.status_code == 404
    assert response.json()["detail"] == "Object does not exist (User, id=69)."


def test_delete_user(user_factory, client):
    user = user_factory.create()

    response = client.delete(f"/users/{user.id}")

    assert response.status_code == 204


def test_delete_user_not_exist(client):
    response = client.delete("/users/69")

    assert response.status_code == 404
    assert response.json()["detail"] == "Object does not exist (User, id=69)."


@patch("app.actions.user.handle_file_upload")
def test_upload_image(save_image_mock, client, user):
    save_image_mock.return_value = "test"
    response = client.post(f"/users/{user.id}/image/", files={"image": ("filename", b"test_img.jpeg", "image/jpeg")})

    assert response.status_code == 200


def test_remove_image(client, user):
    response = client.delete(f"/users/{user.id}/image/")

    assert response.status_code == 200


def test_get_image_user_has_no_image(client, user):
    response = client.get(f"/users/{user.id}/image")

    assert response.status_code == 404
    assert response.json()["detail"] == "The user has no image."


def test_get_image(client, user_factory):
    @contextmanager
    def temp_open(path):
        file = open(path, "wb")
        try:
            yield path
        finally:
            file.close()
            os.remove(path)

    with temp_open("test_img.jpg") as path:
        user = user_factory.create(image=path)
        response = client.get(f"/users/{user.id}/image")

        assert response.status_code == 200
