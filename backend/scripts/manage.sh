#!/usr/bin/env bash

function show_help() {
    echo "Prinder - manage CLI"
    echo "Possible flags:"
    echo "-h/--help       Show help with available commands"
    echo "-c/--create     Populates a database with random data (requires prepared"
    echo "                database - all migration must be applied). In case of"
    echo "                incorrent generated data the quickest solution is flush"
    echo "                database, apply migrations and recall create initial data"
    echo "-f/--flush      Drop all tables (this operation is irreversible!)"
    echo "-m/--migrate    Apply migration stored in 'app/alembic/versions/'. This "
    echo "                step is required before calling 'create'."
}
command=""
args=""

while [[ $# -gt 0 ]]; do
    case $1 in
        -h | --help)
            show_help
            exit 0
            shift
            ;;
        -c | --create)
            command="create-initial-data"
            shift
            ;;
        -f | --flush)
            command="flush"
            shift
            ;;
        -m | --migrate)
            command="migrate"
            shift
            ;;
        *) ;;
    esac
done

if [[ $args -gt 0 ]]; then
    docker compose exec fastapi python backend/cli.py "$command" "$args"
else
    docker compose exec fastapi python backend/cli.py "$command"
fi
