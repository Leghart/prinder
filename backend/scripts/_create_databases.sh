#!/bin/bash

set -eu

function create_databases() {
    database=$1
    password=$2
    echo "Creating user and database '$database' with password '$password'"
    psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
      CREATE USER $database with encrypted password '$password';
      CREATE DATABASE $database;
      ALTER USER $database WITH SUPERUSER;
EOSQL
}


if [ -n "$POSTGRES_MULTIPLE_DATABASES" ]; then
  echo "Multiple database creation requested: $POSTGRES_MULTIPLE_DATABASES"
  for db in $(echo "$POSTGRES_MULTIPLE_DATABASES" | tr ',' ' '); do
    user=$(echo "$db" | awk -F":" '{print $1}')
    pswd=$(echo "$db" | awk -F":" '{print $2}')
    if [[ -z "$pswd" ]]
    then
      pswd=$user
    fi
    echo "user is $user and pass is $pswd"
    create_databases "$user" "$pswd"
  done
  echo "Multiple databases created!"
fi

