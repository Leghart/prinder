#!/usr/bin/env bash

set -xue

cd backend && alembic upgrade head
uvicorn app.main:app --host 0.0.0.0 --reload
