# type: ignore
"""Special file where you can testing queries into production database.

Allows to test factories, creating records, checking factory attributes
and perform every orm commands.
"""
from datetime import datetime

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker

import app.actions as a
import app.factories as f
import app.models as m
from app.database import Base, clear_db, init_db
from app.settings import settings
from tests.conftest import FactoryTestManager

echo = False
# engine = create_engine(settings.DATABASE_URL, echo=echo)
engine = create_engine(settings.DATABASE_URL_TEST)
db = scoped_session(sessionmaker(autoflush=True, bind=engine))


def factory_test(factory):
    fac = factory()
    for attr in dir(fac):
        if not attr.startswith("_"):
            print(attr, ": ", getattr(f, attr))


# print(Base.metadata.tables.key)
# Base.metadata.drop_all(bind=engine)
clear_db(engine),
init_db(engine)

nf = FactoryTestManager(f.SkillFactory)
aaa = nf.create_batch(10)

# print(db.query(m.Language).all())

# from abc import ABC

# from fastapi import APIRouter
# from fastapi_utils.cbv import cbv


# _router = APIRouter()


# @cbv(_router)
# class ModelViewSet(ABC):
#     class Meta:
#         schema = user
#         action = None
#         db = None
#         router = _router
#         _prefix = "users"

#     @Meta.router.get(f"{Meta._prefix}", response_model=list[Meta.schema.ReadSchema])
#     def list(self):
#         return self.Meta.action.list(self.db)
