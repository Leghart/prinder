#!/usr/bin/env python3
import json
from random import choice, randint, sample

import click

from app import factories
from app.database import clear_db, get_main_tables, init_db
from app.utils.auth import encrypt_password

with open("backend/data_range.json") as f:
    ranges = json.load(f)


@click.group(help="CLI tool to manage database")
def cli():
    ...


@click.command()
def flush():
    clear_db()
    click.echo("Database was cleared!")


@click.command()
def migrate():
    init_db()
    click.echo("Database was initiated!")


@click.command()
def create_initial_data():
    click.echo("Creating random data ...")

    all_users = []
    all_languages = []
    all_skills = []
    all_projects = []
    all_permissions = []

    # Creating permissions
    basic_permissions = ("create", "read", "update", "delete", "list")
    basic_tables = get_main_tables()
    basic_tables.remove("permissions")

    for table in basic_tables:
        for perm in basic_permissions:
            all_permissions.append(factories.PermissionFactory(table=table, name=perm))
    click.echo(f"Created permissions ({len(all_permissions)}).")

    # Creating staff
    factories.UserFactory(username="user", password=encrypt_password("user"))
    factories.UserFactory(
        username="admin", password=encrypt_password("admin"), add_permissions=all_permissions, is_superuser=True
    )
    click.echo("Created staff users (admin-admin, user-user).")

    all_languages = factories.LanguageFactory.create_batch(randint(*ranges["languages"]))

    # Creating Users with languages and skills
    for _ in range(randint(*ranges["users"])):
        user = factories.UserFactory(add_languages=sample(all_languages, k=randint(1, 3)))
        skills = factories.SkillFactory.create_batch(randint(*ranges["skills"]), user=user)
        all_skills.extend(skills)
        all_users.append(user)

    click.echo(
        f"Created users ({len(all_users)}) with languages ({len(all_languages)}) and skills ({len(all_skills)})."
    )

    # Creating Projects with technologies
    for _ in range(randint(*ranges["projects"])):
        owner = all_users.pop()
        liked_by = sample(all_users, k=randint(0, 4))
        language = choice(all_languages)
        collaborators = sample(all_users, k=randint(0, 5))
        project = factories.ProjectFactory(
            owner=owner,
            liked_by=liked_by,
            language=language,
            add_collaborators=collaborators,
        )
        factories.TechnologyFactory.create_batch(randint(*ranges["technologies"]), project=project)

        all_projects.append(project)

    click.echo(f"Created projects ({len(all_projects)}).")


if __name__ == "__main__":
    cli.add_command(create_initial_data)
    cli.add_command(flush)
    cli.add_command(migrate)
    cli()
