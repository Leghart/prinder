# Prinder

# Table of Contents

- [Requirements](#requirements)
- [Backend](#backend)
  - [Quick start](#quick-start)
  - [Database](#db)
    - [Migrations](#migrations)
  - [CLI](#cli)
    - [Initial data](#initial-data)
    - [Flushing](#flush)
    - [Migrate](#migrate)
  - [Manual queries](#manual-queries)
  - [Tests](#backend-tests)
- [Frontend](#frontend)
- [Docker](#docker)

# Requirements

- docker compose

# Backend

Backend server was created with FastAPI + pydantic + sqlalchemy.

## Quick start

To start backend server, type:

```
docker compose up fastapi -d
```

It starts a container in background, setups database and starts server.
To go inside a container, type: `docker compose exec fastapi bash`

## Database

There are two databases `prod_db` and `test_db`. In development/production purposes, use `prod_db`. Test version is only for unit/integration/smoke tests, but you can also open it by `manual_query`.
Db is based on PostgresSQL. It has own container `db`. To enter there, type

```
docker compose exec db psql <database> <password_db>
```

(variables from `.env`)

### Migrations

Migrations are responsible for creating/deleting (generally managing) tables in the database. To manage of migrations, used alembic. Each of migration (located in `backend/app/alembic/versions`) are automatically applied when fastapi container is starting. To apply all migrations, type `alembic upgrade head`.
When you make any change in database structure, you have to add it to alembic to maintain uniformity. You can do it, with:

```
alembic revision -m <migration_name>
```

It creates a empty revision file, which has to be completed with upgrade and downgrade steps (necessary to maintain database in case of required rollbacks).
:warning: Important: Never delete created migrations especially on production! :warning:

## CLI

To manage database in simple way, created a CLI (script `manage.sh` is entrypoint for python script `cli.py`, that provides interface for basic operations).
Basic operations are shown below.

### Initial data

Populates the database with random data: users, projects, languages, technologies, skills and permissions. Each time there will be random data (specified in the `data_range.json` file). To call it, type:
`./manage.sh -c` or `./manage.sh --create`.

### Flushing

Removes all information from database. After it you have to reproduce tables structure by calling `./manage.sh -m`. To flush data you can type: `./manage.sh -f` or `./manage.sh --flush`.

### Migrate

Creates entire tables, which are created by sqlalchemy. To made it, type: `./manage.sh -m` or `./manage.sh --migrate`.

## Manual queries

When you implement a new feature, you can test relation by sqlachemy, access to database and everything what you want by using file `manual_query.py`. It connects to db and import whole tables defined by sqlalchemy. You can test factories on production or test database.
:warning: Important: created data will be stored in database until you delete them by special function implemented there or just flush whole db.

## Tests

Whole application is tested with the following tests: smoke, unit, integration. To run tests, type:

```
pytest <path_to_files_with_tests>
```

(you can instead of passing a path, type `.` - it will run all tests).

# Frontend
