import UserService from "../services/users";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { AccessibilityInsetIcon } from "@primer/octicons-react";
import UserForm from "../components/UserForm";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";

const UserPanel = () => {
	const [user, setUser] = useState([]);
	const [showUserForm, setShowUserForm] = useState(false);
	const params = useParams();

	useEffect(() => {
		UserService.fetchUser(params.id).then((data) => setUser(data));
	}, []);

	if (user.length !== 0) {
		return (
			<div className="container-fluid">
				<Button
					variant="primary"
					onClick={() => {
						setShowUserForm(true);
					}}
				>
					Add user
				</Button>

				<div className="row " style={{ margin: 2 + "rem" }}>
					<div className="col-3 text-center">
						<div className="card " style={{ minHeight: 200 + "px" }}>
							<div className="card-body text-center p-3 bg-light rounded-3">
								<h1>{user.username}</h1>
								<AccessibilityInsetIcon size={64} />
							</div>
						</div>
					</div>
					<div className="col-7">
						<div className="card " style={{ minHeight: 200 + "px" }}>
							<Card.Header as="h5">User information</Card.Header>
							<Card.Body className="p-3 bg-light rounded-3">
								<div className="container text-start">
									<div className="row">
										<div className="col-2">Name:</div>
										<div className="col-5">{user.first_name}</div>
									</div>
									<div className="row">
										<div className="col-2">Surname:</div>
										<div className="col-5">{user.last_name}</div>
									</div>
									<div className="row">
										<div className="col-2">Email:</div>
										<div className="col-5">{user.email}</div>
									</div>
									<div className="row">
										<div className="col-2">Bio:</div>
										<div className="col-10">{user.bio}</div>
									</div>
								</div>
							</Card.Body>
						</div>
					</div>
					<div className="col-2">
						<div className="card" style={{ minHeight: 200 + "px" }}>
							<Card.Header as="h5">Language skills</Card.Header>
							<Card.Body className="p-3 bg-light rounded-3">
								{user.languages.map((language, index) => (
									<div className="row" key={index}>
										<div className="col-5">{language.name}</div>
										<div className="col-5">{language.level}</div>
									</div>
								))}
							</Card.Body>
						</div>
					</div>
				</div>

				<div className="row " style={{ margin: 2 + "rem" }}>
					<div className="col-2">
						<div className="card " style={{ minHeight: 400 + "px" }}>
							<Card.Header as="h5">Programing skills</Card.Header>
							<Card.Body className="p-3 bg-light rounded-3">
								{user.skills.map((skill, index) => (
									<div className="row" key={index}>
										<div className="col-10">{skill.name}</div>
										<div className="col-2">{skill.level}</div>
									</div>
								))}
							</Card.Body>
						</div>
					</div>
					<div className="col-10">
						<div className="card" style={{ minHeight: 400 + "px" }}>
							<Card.Header as="h5">Projects</Card.Header>
						</div>
					</div>
				</div>
				<UserForm
					show={showUserForm}
					close={() => {
						setShowUserForm(false);
					}}
				/>
			</div>
		);
	}
};

export default UserPanel;
