import React, { useState } from "react";
import { Button } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import { toast } from "react-toastify";
import Container from "react-bootstrap/Container";
import AdminService from "../services/admin.js";

import "../styles/admin.css";

const AdminView = () => {
	const [permissions, setPermissions] = useState([]);
	const [permissionTable, setPermissionTable] = useState("");
	const [permissionName, setPermissionName] = useState("");

	const getPermissions = () => {
		const data = AdminService.fetchPermissions().then((response) => {
			setPermissions(response);
		});
		return data;
	};

	const createPerm = () => {
		const payload = {
			table: permissionTable,
			name: permissionName,
		};
		AdminService.createPermission(payload)
			.then((response) => {
				toast.success(`Created permission ${response.table}-${response.name}`);
			})
			.catch((err) => {
				toast.error(err.response.data.detail[0].msg);
			});
	};

	return (
		<Container className="container">
			<div className="item">
				<Button onClick={() => getPermissions()}>get all permissions</Button>
				<ul>
					{permissions.map((perm, index) => (
						<li key={index}>
							{perm.table}: {perm.name}
						</li>
					))}
				</ul>
			</div>

			<div className="item">
				<Form>
					<input
						onChange={(c) => setPermissionTable(c.target.value)}
						name="Table"
						placeholder="table"
					></input>
					<input
						onChange={(c) => setPermissionName(c.target.value)}
						name="Name"
						placeholder="name"
					></input>

					<Button
						variant="primary"
						onClick={() => {
							createPerm();
						}}
					>
						Add permission
					</Button>
				</Form>
			</div>
		</Container>
	);
};

export default AdminView;
