import React, { useState, useContext } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import AuthContext from "../authProvider.js";

import "../styles/login.css";

export default function LoginView() {
	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");
	const { loginUser } = useContext(AuthContext);

	return (
		<div className="login-box">
			<h1>Log in</h1>
			<Form>
				<Form.Group
					as={Col}
					controlId="validationCustom01"
					className="mb-3"
					onChange={(e) => setUsername(e.target.value)}
				>
					<Form.Label className="label">Username</Form.Label>
					<Form.Control required type="text" placeholder="Enter username" />
					<Form.Control.Feedback type="invalid">
						Please provide a username
					</Form.Control.Feedback>
				</Form.Group>
				<Form.Group
					className="mb-3"
					onChange={(e) => setPassword(e.target.value)}
				>
					<Form.Label className="label">Password</Form.Label>
					<Form.Control
						required
						type="password"
						onChange={(e) => {
							setPassword(e.target.value);
						}}
					/>
					<Form.Control.Feedback type="invalid">
						Please provide a password
					</Form.Control.Feedback>
				</Form.Group>

				<Button
					variant="primary"
					onClick={() => {
						loginUser(username, password);
					}}
					className="login-button-section"
				>
					Submit
				</Button>
				<div id="create-account">
					You do not have an acconut? <a href="/register">Sign up!</a>
				</div>
			</Form>
		</div>
	);
}
