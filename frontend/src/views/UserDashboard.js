import UserService from "../services/users";
import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";

const CustomNavbar = () => {
	const [users, setUsers] = useState([]);

	useEffect(() => {
		UserService.fetchUsers()
			.then((data) => setUsers(data))
			.catch((err) => {
				toast.error(err.response.data.detail);
			});
	}, []);

	return (
		<ul>
			{users.map((user, index) => (
				<li key={index}>
					<a href={`/user/${user.id}`}>{user.first_name}</a>
				</li>
			))}
		</ul>
	);
};

export default CustomNavbar;
