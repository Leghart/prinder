import Carousel from "react-bootstrap/Carousel";
import Container from "react-bootstrap/Container";

const Home = () => {
	const bgCarouselStyle = {
		backgroundColor: "#373737",
		backgroundPosition: "center",
		backgroundSize: "cover",
		height: "100%",
		textAlign: "center",
		color: "white",
	};

	return (
		<Container fluid className="p-0" style={{ height: "100%" }}>
			<Carousel>
				<Carousel.Item>
					<Container fluid style={bgCarouselStyle}>
						<div className="d-flex h-100 align-items-center justify-content-center">
							<h1>Poczatek</h1>
						</div>
					</Container>
					<Carousel.Caption>
						<h3>First slide</h3>
						<p>Nic nie umiemy</p>
					</Carousel.Caption>
				</Carousel.Item>
				<Carousel.Item>
					<Container fluid style={bgCarouselStyle}>
						<div className="d-flex h-100 align-items-center justify-content-center">
							<h1>Serio</h1>
						</div>
					</Container>

					<Carousel.Caption>
						<h3>Second slide</h3>
						<p>Nic nie potrafimy.</p>
					</Carousel.Caption>
				</Carousel.Item>
				<Carousel.Item>
					<Container fluid style={bgCarouselStyle}>
						<div className="d-flex h-100 align-items-center justify-content-center">
							<h1>Zakonczenie</h1>
						</div>
					</Container>

					<Carousel.Caption>
						<h3>Third slide</h3>
						<p>Nawet koski w cs nie dropniemy</p>
					</Carousel.Caption>
				</Carousel.Item>
			</Carousel>
		</Container>
	);
};

export default Home;
