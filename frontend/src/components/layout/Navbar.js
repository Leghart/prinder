import React, { useContext } from "react";

import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import { useNavigate } from "react-router-dom";
import Navbar from "react-bootstrap/Navbar";
import AuthContext from "../../authProvider";
import "../../styles/navbar.css";

export default function CustomNavbar() {
	const { user, logoutUser } = useContext(AuthContext);
	const navigate = useNavigate();

	return (
		<Navbar bg="dark" variant="dark" sticky="top">
			<Container fluid style={{ marginLeft: 0 }}>
				<Navbar.Brand href="/">Logo placeholder</Navbar.Brand>
				<Nav className="me-auto">
					<Nav.Link href="/">Home</Nav.Link>
					<Nav.Link href="/users">Users</Nav.Link>
					{user ? (
						<Nav.Link
							onClick={() => {
								logoutUser();
							}}
						>
							Logout
						</Nav.Link>
					) : (
						<Nav.Link href="/login">Login</Nav.Link>
					)}
					{user && user.is_superuser ? (
						<Nav.Link href="/admin">Admin panel</Nav.Link>
					) : null}
				</Nav>
			</Container>
			{user ? (
				<div
					className="logged-in"
					onClick={() => {
						navigate(`/user/${user.id}`);
					}}
				>
					{user.username}
				</div>
			) : null}
		</Navbar>
	);
}
