/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import { Modal } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import PropTypes from "prop-types";
import UserService from "../services/users";

export default function UserForm(props) {
	const [validated, setValidated] = useState(false);
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");
	const [bio, setBio] = useState("");
	const [image, setImage] = useState(null);

	const handleSubmit = async (event) => {
		event.preventDefault();

		const form = event.currentTarget;
		if (form.checkValidity() === false) {
			event.stopPropagation();
			setValidated(true);
		} else {
			const formData = new FormData();

			formData.append("image", image);
			const payload = {
				first_name: firstName,
				last_name: lastName,
				email: email,
				username: username,
				password: password,
				bio: bio,
			};

			UserService.createUser(payload).then((data) => {
				console.log(data);
				UserService.uploadImage(data.id, formData).then((data) =>
					console.log("GIT: ", data),
				);
			});

			props.close();
		}
		event.preventDefault();
		event.stopPropagation();
	};

	return (
		<Modal show={props.show} onHide={props.close} centered>
			<Form noValidate validated={validated} onSubmit={handleSubmit}>
				<Modal.Header closeButton>
					<Modal.Title className="ms-auto">New user</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<input
						onChange={(c) => setFirstName(c.target.value)}
						name="first_name"
						placeholder="first name"
					></input>
					<input
						onChange={(c) => setLastName(c.target.value)}
						name="last_name"
						placeholder="last name"
					></input>
					<input
						onChange={(c) => setEmail(c.target.value)}
						name="email"
						placeholder="email"
					></input>
					<input
						onChange={(c) => setUsername(c.target.value)}
						name="username"
						placeholder="username"
					></input>
					<input
						onChange={(c) => setPassword(c.target.value)}
						name="password"
						type="password"
						placeholder="password"
					></input>
					<input
						onChange={(c) => setBio(c.target.value)}
						name="bio"
						placeholder="bio"
					></input>
					<input
						onChange={(e) => setImage(e.target.files[0])}
						name="image"
						type="file"
					></input>
				</Modal.Body>
				<Modal.Footer>
					<Button variant="secondary" onClick={props.close}>
						Close
					</Button>

					<Button variant="primary" type="submit">
						Add user
					</Button>
				</Modal.Footer>
			</Form>
		</Modal>
	);
}

UserForm.propTypes = {
	show: PropTypes.bool,
	close: PropTypes.func,
};
