import React, { createContext, useState, useEffect } from "react";
import jwt_decode from "jwt-decode";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import PropTypes from "prop-types";
import AdminService from "./services/admin";

import "react-toastify/dist/ReactToastify.css";

const AuthContext = createContext();

export default AuthContext;

export const AuthProvider = ({ children }) => {
	const [accessToken, setAccessToken] = useState(() =>
		localStorage.getItem("accessToken")
			? localStorage.getItem("accessToken")
			: null,
	);

	const [user, setUser] = useState(() =>
		localStorage.getItem("accessToken")
			? jwt_decode(localStorage.getItem("accessToken"))
			: null,
	);

	const [loading, setLoading] = useState(true);

	const navigate = useNavigate();

	const loginUser = (username, password) => {
		const formData = new FormData();
		formData.append("username", username);
		formData.append("password", password);

		AdminService.login(formData)
			.then((response) => {
				setAccessToken(response.access_token);
				setUser(jwt_decode(response.access_token).username);
				localStorage.setItem("accessToken", response.access_token);
				navigate(-1);
			})
			.catch(() => {
				toast.error("Invalid credentials");
			});
	};

	const logoutUser = () => {
		setAccessToken(null);
		setUser(null);
		localStorage.removeItem("accessToken");
		navigate("/");
	};

	const contextData = {
		user,
		loginUser,
		logoutUser,
	};

	useEffect(() => {
		if (accessToken) {
			setUser(jwt_decode(accessToken));
		}
		setLoading(false);
	}, [accessToken, loading]);

	return (
		<AuthContext.Provider value={contextData}>
			{loading ? null : children}
		</AuthContext.Provider>
	);
};

AuthProvider.propTypes = { children: PropTypes.array };
