import axios from "axios";

axios.defaults.baseURL = "http://localhost:8000/";

const jsonApi = axios.create({
	headers: {
		"Content-Type": "application/json",
		Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
	},
});

const formApi = axios.create({
	headers: {
		"Content-Type": "multipart/form-data",
		Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
	},
});

export { jsonApi, formApi };
