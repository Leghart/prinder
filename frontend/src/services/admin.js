import { jsonApi, formApi } from "./api";

const AdminService = {
	login: async function (payload) {
		const response = await formApi.post("/token/", payload);
		return response.data;
	},

	fetchPermissions: async function () {
		const response = await jsonApi.get("/admin/permissions/");
		return response.data;
	},

	createPermission: async function (payload) {
		const response = await jsonApi.post("/admin/permissions/", payload);
		return response.data;
	},

	deletePermission: async function (id) {
		const response = await jsonApi.delete(`/admin/permissions/${id}`);
		return response.data;
	},
};

export default AdminService;
