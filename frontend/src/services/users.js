import { formApi, jsonApi } from "./api";

const UserService = {
	createUser: async function (payload) {
		const response = await jsonApi.post("/users/", payload, {});
		return response.data;
	},

	uploadImage: async function (id, payload) {
		const response = await formApi.post(`/users/${id}/image/`, payload, {});
		return response.data;
	},

	fetchUsers: async function () {
		const response = await jsonApi.get("/users/");
		return response.data;
	},

	fetchUser: async function (id) {
		const response = await jsonApi.get(`/users/${id}`);
		return response.data;
	},

	patchUser: async function (update) {
		const response = await jsonApi.patch(`/users/${update.id}`, update.payload);
		return response.data;
	},

	deleteUser: async function (id) {
		const response = await jsonApi.delete(`/users/${id}`);
		return response.data;
	},
};

export default UserService;
