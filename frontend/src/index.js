import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import CustomNavbar from "./components/layout/Navbar";
import NoPage from "./views/NoPage";
import AdminView from "./views/Admin";
import LoginView from "./views/Login";
import Home from "./views/Home";
import UserPanel from "./views/UserPanel";
import UserDashboard from "./views/UserDashboard";
import { AuthProvider } from "./authProvider.js";
import { ToastContainer } from "react-toastify";

import "./assets/global.scss";

export default function App() {
	return (
		<BrowserRouter>
			<AuthProvider>
				<CustomNavbar />
				<Routes>
					<Route index element={<Home />} />
					<Route path="user/:id" element={<UserPanel />} />
					<Route path="users" element={<UserDashboard />} />
					<Route path="admin" element={<AdminView />} />
					<Route path="login" element={<LoginView />} />

					<Route path="*" element={<NoPage />} />
				</Routes>
				<ToastContainer
					position="top-center"
					autoClose={3000}
					hideProgressBar={true}
					theme="colored"
					pauseOnHover
				/>
			</AuthProvider>
		</BrowserRouter>
	);
}

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(<App />);
